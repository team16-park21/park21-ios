//
//  QRScanControllerView.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit
class ExitParkingQRViewController: UIViewController
{
    @IBOutlet weak var imageViewQRCode: UIImageView!
    @IBOutlet weak var callSupport: UIBarButtonItem!
    @IBOutlet weak var salirButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleButtons()
        generateQRCode()
    }
    
    func styleButtons() {
        ButtonFormatting.greenBorderButton(salirButton)
    }
    
    func formatButtons() {
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
        ButtonFormatting.setNavigationBarGreen(self.navigationController!)
    }
    
    func generateQRCode() {
        var qrMessage = ""
        if let userId = AppState.getState().usuario?.id {
            qrMessage.append(userId)
        }
        qrMessage.append(";")
        if let transactionId = AppState.getState().currentTransaction?.transaccionID {
            qrMessage.append(transactionId)
        }
        
        //Encode, generate QR and display
        //Read from https://medium.com/@dominicfholmes/generating-qr-codes-in-swift-4-b5dacc75727c
        let data = qrMessage.data(using: String.Encoding.ascii)
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        qrFilter.setValue(data, forKey: "inputMessage")
        guard let qrImage = qrFilter.outputImage else { return }
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        
        imageViewQRCode.image = UIImage(ciImage: scaledQrImage)
    }
    
    func presentMainView() {
        //Update state
        AppState.getState().userInTransaction = false
        AppState.getState().currentTransaction = nil
        
        if let usr = AppState.getState().usuario {
            //Edit user state
            AppState.getState().usuario?.transaccionActual = ""
            
            //Delete transaction
            UsuariosDB().editTransactionForUserWithID(usr.id, transactionId: "", callback: { successful, err in
                if !successful {
                    print("ERROR WHEN DELETING TRANSACTION FROM USER")
                    print("\(err!)")
                } else {
                    //Replace nav controller stack
                    let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let mainVC : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "AppMainView")
                    if let navVC = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                        navVC.setViewControllers([mainVC], animated: true)
                    }
                }
                
            })
        } else {
            print("COULD NOT DELETE TRANSACTION FROM USER")
        }
        
    }
    
    @IBAction func callAppSupport(_ sender: UIBarButtonItem){
        if let securityPhone = AppState.getState().currentParqueadero?.telefono {
            let url = URL(string:"telprompt://\(securityPhone)")
            UIApplication.shared.open(url!)
        } else {
            let url = URL(string:"telprompt://\(AppState.supportNumer)")
            UIApplication.shared.open(url!)
        }
    }
    
    
    @IBAction func onSalirClicked(_ sender: Any) {
        if AppState.getState().networkStatus {
            self.presentMainView()
        } else {
            Alerts.showErrorAlert("Esta operación requiere de conexión a internet.", presenter: self)
        }
    }
}

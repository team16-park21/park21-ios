//
//  QRCodeGenerator.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Firebase
import UIKit

class ParkingQRCodeViewController: UIViewController {
    @IBOutlet weak var imageViewQRCode: UIImageView!
    @IBOutlet weak var callSupport: UIBarButtonItem!
    
    //User data monitoring
    var userObserver: UserTransactionObserver? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUserObserver()
        generateQRCode()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        userObserver?.dismiss()
    }
    
    func setupUserObserver() {
        userObserver = UserTransactionObserver(onDismissCallback: {
            self.userObserver = nil
        })
    }
    
    func formatButtons() {
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
        ButtonFormatting.setNavigationBarGreen(self.navigationController!)
    }
    
    func generateQRCode() {
        //Read from https://medium.com/@dominicfholmes/generating-qr-codes-in-swift-4-b5dacc75727c
        let qrMessage = AppState.getState().usuario!.id
        let data = qrMessage.data(using: String.Encoding.ascii)
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        qrFilter.setValue(data, forKey: "inputMessage")
        guard let qrImage = qrFilter.outputImage else { return }
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        
        imageViewQRCode.image = UIImage(ciImage: scaledQrImage)
    }
    
    @IBAction func callAppSupport(_ sender: UIBarButtonItem){
        let phoneNumber = "3103068216"
        let url = URL(string:"telprompt://\(phoneNumber)")
        UIApplication.shared.open(url!)
    }
}

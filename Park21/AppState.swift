//
//  State.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Network
import Firebase

class AppState {
    static let pathToSate: String = "AppState_DATA"
    static let supportNumer: String = "3108789573"
    
    //Singleton instance
    private static var state: AppState? = nil
    
    //Network monitoring
    let monitor = NWPathMonitor()
    var networkStatus = false
    
    //Transaction handling
    var transactionIsPayed = false
    var userInTransaction = false
    
    private var _usuario: Usuario? = nil
    var usuario: Usuario? {
        get { return self._usuario }
        set(val) { self._usuario = val }
    }
    
    private var _parqueaderos: [Parqueadero]? = nil
    var parqueaderos: [Parqueadero]? {
        get { return self._parqueaderos }
        set(val) { self._parqueaderos = val }
    }
    
    private var _selectedCar: Carro? = nil
    var selectedCar: Carro? {
        get { return self._selectedCar }
        set(val) { self._selectedCar = val }
    }
    
    private var _editingCarData: Carro? = nil
    var editingCarData: Carro? {
        get { return self._editingCarData }
        set(val) { self._editingCarData = val }
    }
    
    private var _currentTransaction: Transaccion? = nil
    var currentTransaction: Transaccion? {
        get { return self._currentTransaction }
        set(val) { self._currentTransaction = val }
    }
    
    private var _currentParqueadero: Parqueadero? = nil
    var currentParqueadero: Parqueadero? {
        get { return self._currentParqueadero }
        set(val) { self._currentParqueadero = val }
    }
    
    private var _family: Familia? = nil
    var family: Familia? {
        get { return self._family}
        set(val){self._family = val}
    }
    
    var factura: Factura? = nil
    
    struct DumpableState: Codable {
        var usuario: Usuario?
        var parqueaderos: [Parqueadero]?
        var currentTransaction: Transaccion?
        var currentParqueadero: Parqueadero?
        var family: Familia?
    }
    
    private init() {
        let monitor = NWPathMonitor()
            monitor.pathUpdateHandler = { path in
            if path.status == .satisfied {
                self.networkStatus = true
                
            } else {
                self.networkStatus = false
                }
                
            }
            let queue = DispatchQueue(label: "Monitor")
            monitor.start(queue: queue)
    }
    
    //Get instance for singleton
    public static func getState() -> AppState {
        if let state = state {
            return state
        } else {
            state = AppState()
            return state!
        }
    }
    
    func updateParqueaderos(newParqueaderos: [Parqueadero]) {
        self.parqueaderos = newParqueaderos
    }
    
    static func clean() {
        let backup = AppState.state?.parqueaderos
        AppState.state = AppState()
        //Prevent erase of parqueaderos information since this is not session related
        AppState.state?.parqueaderos = backup
        FileManagementUtil().writeToFile(file: AppState.pathToSate, payload: "")
    }
    
    func dumpState() {
        print("DUMPING")
        let copy = DumpableState(usuario: self.usuario, parqueaderos: self.parqueaderos, currentTransaction: self.currentTransaction, currentParqueadero: self.currentParqueadero, family: self.family)
        
        do {
            let data = try JSONEncoder().encode(copy)
            let payload = String(data: data, encoding: .utf8)
            FileManagementUtil().writeToFile(file: AppState.pathToSate, payload: payload!)
        } catch {
            print("ERROR WHILE DUMPING STATE")
            return
        }
    }
    
    func loadState() {
        guard let payload = FileManagementUtil().readBackupStateFile(file: AppState.pathToSate) else {
            print("No available state to load")
            return
        }
        do {
            let dump = try JSONDecoder().decode(DumpableState.self, from: payload.data(using: .utf8)!)
            self._usuario = dump.usuario
            self._parqueaderos = dump.parqueaderos
            self._currentTransaction = dump.currentTransaction
            self._currentParqueadero = dump.currentParqueadero
            self._family = dump.family
        } catch {
            print("ERROR READING STATE FROM FILE")
        }
    }
}

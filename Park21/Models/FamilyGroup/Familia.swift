//
//  File.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/28/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation

class Familia: Codable {
    private var _id: String
    var id: String {
        get{ self._id }
        set(val) { self._id = val }
    }
    
    private var _users: [Integrante]
    var users: [Integrante] {
        get{self._users}
        set(val){self._users = val}
    }
    
    struct Integrante: Codable {
        var nombre: String
        var id: String
    }
    
    init(id: String, integrantes: [[String:String]])
    {
        self._id = id
        self._users = []
        for integrante in integrantes {
            let miembro = Integrante(nombre: integrante["nombre"]!,
                                     id: integrante["id"]!)
            _users.append(miembro)
        }
    }
    
    init(id: String, integrante: Integrante)
    {
        self._id = id
        self._users = []
        _users.append(integrante)
    }
}



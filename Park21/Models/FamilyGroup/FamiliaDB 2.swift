//
//  FamiliaDB.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/28/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Firebase
class FamiliaDB {
    let collectionName = "familias"
    
    func getFamilyWithId(_ id: String, callback: @escaping (Familia?) -> Void)
    {
        let db = Firestore.firestore()
        let docRef = db.collection(self.collectionName).document(id)
        print("Me llamaron yeii")
        docRef.getDocument(source: .server) { doc, err in
            if err != nil {
                print("There was an error while fetching family with id \(id)")
            } else {
                if let data = doc?.data() {
                    let familia = Familia(id: doc!.documentID ,integrantes: data["integrantes"] as! [[String:String]])
                    callback(familia)
                } else {
                    print("There was no family with id \(id) to fetch")
                    callback(nil)
                }
            }
        }
    }
    
    func deleteMemberWithIDFromFamily(_ id: String, _ family : Familia, callback: @escaping (Bool, Error?) -> Void)
    {
        let db = Firestore.firestore()
        let newMembers = family.users.filter({user in
            return user.id != id})
        let newMembersDict = getMembersArrayAsDict(newMembers)
        
        UsuariosDB().editFamilyGroupForUserWithID(id, familyId: "", callback: {successful, err in
            if successful{
                print(family.id)
                db.collection(self.collectionName).document(family.id).updateData(["integrantes": newMembersDict]){err in
                    if let err = err {
                        callback(false, err)
                    } else {
                        callback(true, nil)
                    }
                }
            }
            else{
                print("Se generó un error al eliminar un miembro, por favor intentelo de nuevo")
                callback(false, err)
            }
        })
    }
    
    func addMemberToFamilyWithId(idFamily: String, integrante : Familia.Integrante, _ family: Familia, callback: @escaping (Bool, Error?) -> Void)
    {
        let db = Firestore.firestore()
        var members = family.users
        members.append(integrante)
        family.users = members
        let membersAsDic = getMembersArrayAsDict(members)
        db.collection(self.collectionName).document(idFamily).updateData(["integrantes": membersAsDic]){err in
            if let err = err {
                callback(false, err)
            } else {
                callback(true, nil)
            }
        }
    }
    
    func createFamilyGroup(integrante : Familia.Integrante, callback: @escaping (Familia?, String?, Bool, Error?) -> Void)
    {
        var ref: DocumentReference? = nil
        var familyGroupId = ""
        
        let db = Firestore.firestore()
        let members = getMembersArrayAsDict([integrante])
        let familia = Familia(id: "", integrante: integrante)
        
        ref = db.collection("familias").addDocument(data: ["integrantes": members]){ err in
            if let err = err
            {
                print("Error creating a new family group: \(err)")
                callback(nil, nil, false, err)
            }
            else{
                familyGroupId = ref!.documentID
                familia.id = familyGroupId
                callback(familia, familyGroupId, true, nil)
            }
        }
    }
    
    func createObserverToFamilyWithID(_ familyId: String, onSnapshotCallback: @escaping (Familia) -> Void) -> ListenerRegistration {
        let db = Firestore.firestore()
        
        let observer = db.collection(self.collectionName).document(familyId)
            .addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("Error fetching document family: \(error!)")
                    return
                }
                guard let data = document.data() else {
                    print("Document family data was empty.")
                    return
                }
                let familia = Familia(id: document.documentID, integrantes: data["integrantes"] as! [[String:String]])
                onSnapshotCallback(familia)
        }
        
        return observer
    }
}

extension FamiliaDB {
    
    func getMembersArrayAsDict (_ members: [Familia.Integrante]) -> [[String : String]]{
        var arreglo = [[String : String]]()
        for member in members {
            var intDict: [String : String] = [:]
            intDict["id"] = member.id
            intDict["nombre"] = member.nombre
            arreglo.append(intDict)
        }
        return arreglo
    }
    
    
}

//
//  Transaccion.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/13/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation

class Transaccion: Codable {
    private var _transaccionID: String? = nil
    var transaccionID: String? {
        get { return self._transaccionID }
        set(val) { self._transaccionID = val }
    }
    
    private var _usuarioID: String? = nil
    var usuarioID: String? {
        get { return self._usuarioID }
        set(val) { self._usuarioID = val }
    }
    
    private var _parqueaderoID: String? = nil
    var parqueaderoID: String? {
        get { return self._parqueaderoID }
        set(val) { self._parqueaderoID = val }
    }
    
    private var _fechaEntrada: Date? = nil
    var fechaEntrada: Date? {
        get { return self._fechaEntrada }
        set(val) { self._fechaEntrada = val }
    }
    
    private var _fechaSalida: Date? = nil
    var fechaSalida: Date? {
        get { return self._fechaSalida }
        set(val) { self._fechaSalida = val }
    }
    
    private var _tiempo: Int? = nil
    var tiempo: Int? {
        get { return self._tiempo }
        set(val) { self._tiempo = val }
    }
    
    private var _pago: Pago? = nil
    var pago: Pago? {
        get { return self._pago }
        set(val) { self._pago = val }
    }
    
    private var _puestoParqueo: PuestoParqueo? = nil
    var puestoParqueo: PuestoParqueo? {
        get { return self._puestoParqueo }
        set(val) { self._puestoParqueo = val }
    }
    
    struct Pago: Codable {
        var metodo: String
        var valor: Int
    }
    
    struct PuestoParqueo: Codable {
        var id_puesto: String
        var instrucciones: String
    }
    
    init(transaccionID: String, usuarioID: String, parqueaderoID: String, fechaEntrada: Date, fechaSalida: Date, tiempo: Int, pago: [String: Any], puestoParqueo: [String: Any]) {
        self._transaccionID = transaccionID
        self._usuarioID = usuarioID
        self._parqueaderoID = parqueaderoID
        self._fechaEntrada = fechaEntrada
        self._fechaSalida = fechaSalida
        self._tiempo = tiempo
        self._pago = Pago(metodo: pago["metodo"] as! String,
                          valor: pago["valor"] as! Int)
        self._puestoParqueo = PuestoParqueo(id_puesto: puestoParqueo["id_puesto"] as! String,
                                            instrucciones: puestoParqueo["instrucciones"] as! String)
    }
}

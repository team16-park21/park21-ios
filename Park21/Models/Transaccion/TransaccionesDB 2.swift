
//
//  TransaccionDB.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/13/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Firebase

class TransaccionesDB {
    let collectionName = "transacciones"
    
    func getTransaccionWithID(_ transaccionID: String, callback: @escaping (Transaccion?) -> Void) {
        let db = Firestore.firestore()
        
        let docRef = db.collection(self.collectionName).document(transaccionID)
        docRef.getDocument(source: .server) { doc, err in
            if err != nil {
                print("There was an error while fetching transaction with id \(transaccionID)")
            } else {
                if let data = doc?.data() {
                    //Converting fecha entrada
                    let timestampEntrada = data["fechaEntrada"] as! Timestamp
                    let fechaEntrada = timestampEntrada.dateValue()
                    //Converting fecha salida
                    let timestampSalida = data["fechaSalida"] as! Timestamp
                    let fechaSalida = timestampSalida.dateValue()
                    
                    
                    let transaccion = Transaccion(transaccionID: doc!.documentID,
                                                  usuarioID: data["usuario"] as! String,
                                                  parqueaderoID: data["parqueadero"] as! String,
                                                  fechaEntrada: fechaEntrada,
                                                  fechaSalida: fechaSalida,
                                                  tiempo: data["tiempo"] as! Int,
                                                  pago: data["pago"] as! [String : Any],
                                                  puestoParqueo: data["puestoParqueo"] as! [String : Any])
                    callback(transaccion)
                } else {
                    print("There was no transaction with id \(transaccionID) to fetch")
                    callback(nil)
                }
            }
        }
    }
    
    func updateTransaccionWithID(_ transaccionID: String, payload: Transaccion, updateFields: [String], callback: @escaping (Bool, Error?) -> Void) {
        let db = Firestore.firestore()
        
        //Generate patch fields for update call
        let preFiltered = getTransactionAsDict(payload)
        //Filter fields based on what wants to be patched
        let updateArray = DTOManagement().filterDict(dict: preFiltered, filters: updateFields)
        
        //Make update call
        let docRef = db.collection(self.collectionName).document(transaccionID)
        docRef.updateData(updateArray) {err in
            if let err = err {
                callback(false, err)
            } else {
                callback(true, nil)
            }
        }
    }
}

//MARK: TransaccionesDB utilities
extension TransaccionesDB {
    func getTransactionAsDict(_ transaction: Transaccion) -> [String: Any] {
        var dict = [String: Any]()
        dict["fechaEntrada"] = transaction.fechaEntrada
        dict["usuario"] = transaction.usuarioID
        dict["parqueadero"] = transaction.parqueaderoID
        dict["tiempo"] = transaction.tiempo
        dict["pago"] = transaction.pago
        dict["puestoParqueo"] = transaction.puestoParqueo
        
        return dict
    }
}

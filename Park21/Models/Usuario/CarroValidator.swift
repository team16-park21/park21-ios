//
//  CarroValidation.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/12/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation

class CarroValidator {
    //Mensajes para placa
    let notAPlate = "La placa ingresada no es válida, debe tener tres letras al comienzo y tres números al final (e.g. ABC123)."
    let plateSizeError = "El tamaño de la placa ingresada no es válido, por favor valide que hayan tres letras al comienzo y tres números al final (e.g. ABC123)."
    let emptyPlate = "La placa no puede estar vacia."

    //Mensajes para marca
    let notABrand = "La marca no puede tener caracteres especiales o números."
    let emptyBrand = "La marca no puede estar vacia."
    let brandTooBig = "La marca no puede tener más de 65 caracteres."
    
    func isPlateValid(_ plate: String) -> (isValid: Bool, message: String) {
        if plate == "" {
            return (false, self.emptyPlate)
        }
        
        if plate.count > 6 {
            return (false, self.plateSizeError)
        }
        
        let pattern = "[a-zA-Z]{3}[0-9]{3}"
        if let result = plate.range(of: pattern, options:.regularExpression) {
            print("Result = \(result)")
            return (true, "OK")
        } else {
            return (false, self.notAPlate)
        }
    }
    
    func isBrandValid(_ brand: String) -> (isValid: Bool, message: String) {
        if brand == "" {
            return (false, self.emptyBrand)
        }
        
        if brand.count > 65 {
            return (false, self.brandTooBig)
        }
        
        let brandRegex = "[a-zA-Z]{1,65}"
        if let result = brand.range(of: brandRegex, options: .regularExpression)
        {
            print("Result was \(result)")
            return (true, "OK")
        } else {
            return (false, self.notABrand)
        }
    }
    
    func isModelValid(_ model: String) -> (isValid: Bool, message: String) {
        if model == "" {
            return (false, "El modelo no puede ser vacio.")
        }
        
        let pattern = "[0-9][0-9][0-9][0-9]"
        if let result = model.range(of: pattern, options:.regularExpression) {
            print("Matched model via regex \(model)")
            print("Result = \(result)")
            
            if let modeloAsInt = Int(model) {
                if modeloAsInt > 2025 || modeloAsInt < 1950 {
                    return (false, "El modelo no puede ser un año mayor a 2025 ó menor a 1950")
                } else {
                    return (true, "OK")
                }
            } else {
                return (false, "El modelo no puede contener caracteres especiales o letras.")
            }
        } else {
            return (false, "El modelo no puede contener caracteres especiales o letras.")
        }
    }
}

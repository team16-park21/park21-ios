//
//  Carro.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation

class Carro: Codable {
    private var _marca: String
    var marca: String {
        get { return _marca }
        set(val) { self._marca = val.uppercased() }
    }
    private var _modelo: String
    var modelo: String {
        get { return _modelo }
        set(val) { self._modelo = val }
    }
    private var _placa: String
    var placa: String {
        get { return _placa }
        set(val) { self._placa = val.uppercased() }
    }
    
    init(marca: String, modelo: String, placa: String) {
        self._marca = marca.uppercased()
        self._modelo = modelo
        self._placa = placa.uppercased()
    }
}

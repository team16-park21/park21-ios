//
//  UsuarioValidator.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/13/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation

class UsuarioValidator {
    //Mensajes para correo
    let emailTooLong = "El correo no puede tener más de 65 caracteres."
    let notAnEmail = "El correo ingresado no tiene formato de correo (e.g. usuario@dominio.com)."
    let emptyEmail = "El correo no puede estar vacio."
    
    //Mensajes para telefono
    let phoneSizeError = "El teléfono no puede tener más de 15 caracteres ó menos de 7 caracteres. Debe ser un teléfono fijo o celular."
    let emptyPhone = "El teléfono no puede estar vacio."
    let notAPhone = "El teléfono no debe tener letras o caracteres especiales."
    
    //Mensajes para cédula
    let cedulaTooLong = "La cédula no puede tener más de 15 caracteres."
    let emptyCedula = "La cédula no puede ser vacia."
    let notACedula = "La cédula no puede tener caracteres especiales o letras."
    
    //Mensajes para edad
    let emptyEdad = "La edad no puede ser vacia."
    let ageNotInRange = "La aplicación no acepta usuarios menores de edad. La edad del usuario no puede superar los 150 años."
    let notAnAge = "La edad no puede tener letras o caracteres especiales."
    
    //Mensajes para nombre
    let nameSizeError = "El nombre no puede tener más de 65 ó menos de 3 caracteres."
    let notAName = "El nombre no puede tener numeros o caracteres especiales."
    
    //Mensajes para genero
    let emptyGender = "Debe de seleccionar un género."
    
    //Mesnajes para password
    let passwordSizeError = "La contraseña no puede tener más de 50 caracteres ó menos de 6 caracteres."
    let passwordEmpty = "La contraseña no puede ser vacía."
    
    func isMailValid (_ mail : String) -> (isValid: Bool, message: String)
    {
        if mail == "" {
            return (false, self.emptyEmail)
        }
        
        if mail.count > 65 {
            return (false, self.emailTooLong)
        }
        
        let mailRegex =
        "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]"
        if let result = mail.range(of: mailRegex, options:.regularExpression) {
            print("Result was \(result)")
            return (true, "OK")
        } else {
            return (false, self.notAnEmail)
        }
    }
    
    func isPasswordValid(_ password: String) -> (isValid: Bool, message: String) {
        
        if password == "" {
            return (false, self.passwordEmpty)
        }
        
        if password.count > 50 || password.count < 6 {
            return (false, self.passwordSizeError)
        }
        
        return (true, "OK")
    }
    
    func isPhoneValid (_ phone : String) -> (isValid: Bool, message: String)
    {
        if phone.count > 15 || phone.count < 7 {
            return (false, self.phoneSizeError)
        }
        
        if phone == "" {
            return (false, self.emptyPhone)
        }
        let phoneRegex = "[0-9]{5,10}"
        if let result = phone.range(of: phoneRegex, options: .regularExpression)
        {
            print("Result was \(result)")
            return (true, "OK")
        }
        return (false, self.notAPhone)
    }
    
    func isCedulaValid(_ cedula : String) -> (isValid: Bool, message: String)
    {
        if cedula.count > 15 {
            return (false, self.cedulaTooLong)
        }
        
        if cedula == "" {
            return (false, self.emptyCedula)
        }
        
        let cedulaRegex = "[0-9]"
        if let result = cedula.range(of: cedulaRegex, options: .regularExpression)
        {
            print("Result was \(result)")
            return (true, "OK")
        }
        return (false, self.notACedula)
    }
    
    func isAgeValid(_ age: String) -> (isValid: Bool, message: String)
    {
        if age == "" {
            return (false, self.emptyEdad)
        }
        
        let ageRegex = "[0-9]"
        if age.range(of: ageRegex, options: .regularExpression) == nil
        {
            return (false, self.notAnAge)
        }
        
        if let ageAsInt = Int(age) {
            if ageAsInt > 150 || ageAsInt < 15{
                return (false, self.ageNotInRange)
            } else {
                return (true, "OK")
            }
        } else {
            return (false, self.notAnAge)
        }
    }
    
    func isNameValid(_ name : String) -> (isValid: Bool, message: String)
    {
        if name.count > 65 || name.count < 3 {
            return (false, self.nameSizeError)
        }
        
        let nameRegex = "[a-zA-Z]"
        if let result = name.range(of: nameRegex, options: .regularExpression)
        {
            print("Result was \(result)")
            return (true, "OK")
        } else {
            return (false, self.notAName)
        }
    }
    
    func isGenderValid (_ gender: String) -> (isValid: Bool, message: String)
    {
        if ["Masculino", "Femenino", "Otro", "Prefiero no decir"].contains(gender) {
            return (true, "OK")
        }
        
        return (false, self.emptyGender)
    }
}

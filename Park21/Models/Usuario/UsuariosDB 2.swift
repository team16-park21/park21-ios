//
//  Persona.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/5/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Firebase

class UsuariosDB  {
    let collectionName = "usuarios"
    
    func getUserWithEmail(_ email: String, callback: @escaping (Usuario?) -> Void) -> Void {
        let db = Firestore.firestore()
        
        print("Getting user with email \(email)")
        let query = db.collection(self.collectionName).whereField("correo", isEqualTo: email)
        query.getDocuments() { (querySnapshot, err) in
            if err != nil {
                print("Error when searching for user with email \(email)")
                print("Error: \(err!)")
            } else if querySnapshot!.documents.count == 0 {
                print("No users found with email \(email)")
                callback(nil)
            } else {
                print("Fetch for usuarios was OKAY")
                let data = querySnapshot!.documents[0].data()
                let user = Usuario(id: querySnapshot!.documents[0].documentID,
                                   correo: data["correo"] as! String,
                                   cedula: data["cedula"] as! String,
                                   nombre: data["nombre"] as! String,
                                   telefono: data["telefono"] as! String,
                                   genero: data["genero"] as! String,
                                   edad: data["edad"] as! Int,
                                   carData: data["carros"] as! [[String: String]],
                                   facturas: data["facturas"] as! [String],
                                   transaccionActual: data["transaccionActual"] as! String,
                                   familyGroupId: data["familyGroup"] as! String)
                callback(user)
            }
        }
    }
    
    func getUserWithID(_ userID: String, callback: @escaping (Usuario?) -> Void) -> Void {
        let db = Firestore.firestore()
        
        print("Getting user with id \(userID)")
        let docRef = db.collection(self.collectionName).document(userID)
        docRef.getDocument(source: .server) { (document, err) in
            if err != nil {
                print("Error when searching for user with id \(userID)")
                print("Error: \(err!)")
            } else {
                print("Fetch for usuarios was OKAY")
                let data = document!.data()!
                let user = Usuario(id: document!.documentID,
                                   correo: data["correo"] as! String,
                                   cedula: data["cedula"] as! String,
                                   nombre: data["nombre"] as! String,
                                   telefono: data["telefono"] as! String,
                                   genero: data["genero"] as! String,
                                   edad: data["edad"] as! Int,
                                   carData: data["carros"] as! [[String: String]],
                                   facturas: data["facturas"] as! [String],
                                   transaccionActual: data["transaccionActual"] as! String,
                                   familyGroupId: data["familyGroup"] as! String)
                callback(user)
            }
        }
    }
    
    func createUser(_ usuario: Usuario, callback: @escaping (Bool, Error?) -> Void) -> Void {
        self.getUserWithEmail(usuario.correo, callback: {usr in
            if usr != nil {
                callback(false, nil)
            } else {
                let db = Firestore.firestore()
                print("Creating new user")
                
                let data = self.getUserAsDict(usuario)
                print("NEW USER\n \(data)")
                db.collection(self.collectionName).addDocument(data: data) {err in
                    if err != nil {
                        print("Error while creating new user")
                        callback(false, err)
                    } else {
                        print("Create for user was OK")
                        callback(true, nil)
                    }
                }
            }
        })
    }
    
    func createObserverToUserWithID(_ userId: String, onSnapshotCallback: @escaping (Usuario) -> Void) -> ListenerRegistration {
        let db = Firestore.firestore()
        
        let observer = db.collection(self.collectionName).document(userId)
            .addSnapshotListener { documentSnapshot, error in
                guard let document = documentSnapshot else {
                    print("Error fetching document user: \(error!)")
                    return
                }
                guard let data = document.data() else {
                    print("Document user data was empty.")
                    return
                }
                let usr = Usuario(id: document.documentID, data: data)
                onSnapshotCallback(usr)
        }
        
        return observer
    }
    
    //MARK: Manage User Data
    func editBasicDataForUserWithID(_ userId: String, newUserData: Usuario, callback: @escaping (Bool, Error?) -> Void) -> Void {
        let db = Firestore.firestore()
        print("Editing data for user with ID \(userId)")
        
        var updatedRef = [String: Any]()
        updatedRef["nombre"] = newUserData.nombre
        updatedRef["genero"] = newUserData.genero
        updatedRef["telefono"] = newUserData.telefono
        updatedRef["edad"] = newUserData.edad
        
        db.collection(self.collectionName).document(userId).updateData(updatedRef) {err in
            if let err = err {
                callback(false, err)
            } else {
                callback(true, nil)
            }
        }
    }
    
    func editTransactionForUserWithID(_ userId: String, transactionId: String, callback: @escaping (Bool, Error?) -> Void) -> Void {
        let db = Firestore.firestore()
        print("Editing data for user with ID \(userId)")
        
        var updatedRef = [String: Any]()
        updatedRef["transaccionActual"] = transactionId
        
        db.collection(self.collectionName).document(userId).updateData(updatedRef) {err in
            if let err = err {
                callback(false, err)
            } else {
                callback(true, nil)
            }
        }
    }
    
    func editFamilyGroupForUserWithID(_ userId: String, familyId: String, callback: @escaping(Bool, Error?) -> Void) ->Void {
        let db = Firestore.firestore()
        var updateRef = [String : Any]()
        updateRef["familyGroup"] = familyId
        db.collection(self.collectionName).document(userId).updateData(updateRef) {err in
            if let err = err {
                callback(false, err)
            } else {
                callback(true, nil)
            }
        }
        
    }
    
    //MARK: Manage Cars Data for user
    func deleteCarFromUser(_ usuario: Usuario, carToDelete: Carro, callback: @escaping (Bool, Error?) -> Void) -> Void {
        let db = Firestore.firestore()
        print("Deleting car with plate \(carToDelete.placa) from user \(usuario.correo)")
        
        let nuevosCarros = usuario.carros.filter({carro in
            return carro.placa != carToDelete.placa
        })
        let updateArray = getCarsArrayAsDict(nuevosCarros)
        
        db.collection(self.collectionName).document(usuario.id).updateData(["carros": updateArray]) {err in
            if let err = err {
                callback(false, err)
            } else {
                callback(true, nil)
            }
        }
    }
    
    func addCarToUser(_ usuario: Usuario, carToAdd: Carro, callback: @escaping (Bool, Error?) -> Void) -> Void {
        let db = Firestore.firestore()
        print("Adding car with plate \(carToAdd.placa) to user \(usuario.correo)")
        
        var nuevosCarros = usuario.carros
        nuevosCarros.append(carToAdd)
        let updateArray = getCarsArrayAsDict(nuevosCarros)
        
        db.collection(self.collectionName).document(usuario.id).updateData(["carros": updateArray]) {err in
            if let err = err {
                callback(false, err)
            } else {
                callback(true, nil)
            }
        }
    }
    
    func editCarFromUser(_ usuario: Usuario, originalCar: Carro, editedCar: Carro, callback: @escaping (Bool, Error?) -> Void) -> Void {
        let db = Firestore.firestore()
        print("Editing car with plate \(originalCar.placa) to user \(usuario.correo)")
        
        var nuevosCarros = [Carro]()
        for car in usuario.carros {
            if car.placa == originalCar.placa {
                nuevosCarros.append(editedCar)
            } else {
                nuevosCarros.append(car)
            }
        }
        
        let updateArray = getCarsArrayAsDict(nuevosCarros)
        
        db.collection(self.collectionName).document(usuario.id).updateData(["carros": updateArray]) {err in
            if let err = err {
                callback(false, err)
            } else {
                callback(true, nil)
            }
        }
    }
}

//MARK: UsuariosDB utilities
extension UsuariosDB {
    func getCarsArrayAsDict(_ data: [Carro]) -> [[String: String]] {
        var dict = [[String: String]]()
        for car in data {
            var carDict: [String: String] = [:]
            carDict["placa"] = car.placa
            carDict["modelo"] = car.modelo
            carDict["marca"] = car.marca
            
            dict.append(carDict)
        }
        
        return dict
    }
    
    func getUserAsDict(_ usuario: Usuario) -> [String: Any] {
        var dict = [String: Any]()
        dict["cedula"] = usuario.cedula
        dict["correo"] = usuario.correo
        dict["edad"] = usuario.edad
        dict["genero"] = usuario.genero
        dict["nombre"] = usuario.nombre
        dict["telefono"] = usuario.telefono
        dict["carros"] = self.getCarsArrayAsDict(usuario.carros)
        dict["facturas"] = usuario.facturas
        dict["transaccionActual"] = usuario.transaccionActual
        dict["familyGroup"] = usuario.familyGroupId
        
        return dict
    }
}

//
//  Usuario.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/5/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation

class Usuario: Codable {
    private var _id: String
    var id: String {
        get { return _id }
        set(val) { _id = val }
    }
    
    private var _correo: String
    var correo: String {
        get { return self._correo }
        set(val) { self._correo = val }
    }
    
    private var _cedula: String
    var cedula: String {
        get { return self._cedula }
        set(val) { self._cedula = val }
    }
    
    private var _nombre: String
    var nombre: String {
        get { return self._nombre }
        set(val) { self._nombre = val }
    }
    
    private var _telefono: String
    var telefono: String {
        get { return self._telefono }
        set(val) { self._telefono = val }
    }
    
    private var _genero: String
    var genero: String {
        get { return self._genero }
        set(val) { self._genero = val }
    }
    
    private var _edad: Int
    var edad: Int {
        get { return self._edad }
        set(val) { self._edad = val }
    }
    
    private var _carros: [Carro]
    var carros: [Carro] {
        get { return self._carros }
        set(val) { self._carros = val }
    }
    
    private var _facturas: [String]
    var facturas: [String] {
        get { return self._facturas }
        set(val) { self._facturas = val }
    }
    
    private var _transaccionActual: String
    var transaccionActual: String {
        get { return self._transaccionActual }
        set(val) { self._transaccionActual = val }
    }
    
    private var _familyGroupId: String
    var familyGroupId: String {
        get {return self._familyGroupId}
        set(val){self._familyGroupId = val}
    }
    
    init(id: String, correo: String, cedula: String, nombre: String, telefono: String, genero: String, edad: Int, carData: [[String: String]], facturas: [String], transaccionActual: String, familyGroupId: String) {
        self._id = id
        self._correo = correo
        self._cedula = cedula
        self._nombre = nombre
        self._telefono = telefono
        self._genero = genero
        self._edad = edad
        self._carros = []
        for data in carData {
            self._carros.append(Carro(marca: data["marca"]!,
                                modelo: data["modelo"]!,
                                placa: data["placa"]!))
        }
        self._facturas = facturas
        self._transaccionActual = transaccionActual
        self._familyGroupId = familyGroupId
    }
    
    init (id: String, data: [String: Any]) {
        self._id = id
        self._correo = data["correo"] as! String
        self._cedula = data["cedula"] as! String
        self._nombre = data["nombre"] as! String
        self._telefono = data["telefono"] as! String
        self._genero = data["genero"] as! String
        self._edad = data["edad"] as! Int
        self._carros = []
        let carData = data["carros"] as! [[String: String]]
        for data in carData {
            self._carros.append(Carro(marca: data["marca"]!,
                                modelo: data["modelo"]!,
                                placa: data["placa"]!))
        }
        self._facturas = data["facturas"] as! [String]
        self._transaccionActual = data["transaccionActual"] as! String
        self._familyGroupId = data["familyGroup"] as! String
    }
}

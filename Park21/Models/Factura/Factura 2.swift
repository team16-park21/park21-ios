//
//  Factura.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 11/9/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation

class Factura {
    private var _id: String
    var id: String {
        get { return _id }
        set(val) { _id = val }
    }
    
    private var _establecimiento: String
    var establecimiento: String {
        get { return self._establecimiento }
        set(val) { self._establecimiento = val }
    }
    
    private var _valor: Int
    var valor: Int {
        get { return self._valor }
        set(val) { self._valor = val }
    }
    
    private var _fecha: Date
    var fecha: Date {
        get { return self._fecha }
        set(val) { self._fecha = val }
    }
    
    private var _userId: String
    var userId: String {
        get { return self._userId }
        set(val) { self._userId = val }
    }
    
    init(id: String, valor: Int, establecimiento: String, fecha: Date, userId: String) {
        self._id = id
        self._valor = valor
        self._establecimiento = establecimiento
        self._fecha = fecha
        self._userId = userId
    }
}

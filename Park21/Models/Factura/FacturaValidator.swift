//
//  FacturaValidator.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 11/9/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation

class FacturaValidator {
    //Mensajes valor
    let emptyValor = "El valor de la factura no puede ser vacio."
    let notAValor = "El valor de la factura solo puede incluir números."
    let valorTooBig = "Las facturas cuyo valor sea superior a los 10'000,000 COP deben ser registradas en el centro de información."
    let valorTooSmall = "No se pueden registrar facturas cuyo valor sea inferir a 2,000 COP."
    
    //Mensajes establecimiento
    let establecimientoSizeError = "El nombre del establecimiento no puede tener más de 100 caracteres ó menos de 4 caracteres."
    let establecimientoEmpty = "El nombre del establecimiento no puede ser vacio."
    
    func isValorValid(_ valor: String) -> (isValid: Bool, message: String) {
        
        if valor == "" {
            return (false, self.emptyValor)
        }
        
        let valorRegex = "[0-9]"
        if valor.range(of: valorRegex, options: .regularExpression) == nil
        {
            return (false, self.notAValor)
        }
        
        if let valorAsInt = Int(valor) {
            if valorAsInt > 10000000 {
                return (false, self.valorTooBig)
            } else if valorAsInt < 5000 {
                return (false, self.valorTooSmall)
            } else {
                return (true, "OK")
            }
        } else {
            return (false, self.notAValor)
        }
    }
    
    func isEstablecimientoValid(_ establecimiento: String) -> (isValid: Bool, message: String) {
        if establecimiento.count > 100 || establecimiento.count < 4 {
            return (false, self.establecimientoSizeError)
        }
        
        if establecimiento == "" {
            return (false, self.establecimientoEmpty)
        }
        
        return (true, "OK")
    }
}

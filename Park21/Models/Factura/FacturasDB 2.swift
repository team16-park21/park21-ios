//
//  FacturasDB.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 11/9/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Firebase

class FacturasDB {
    let collectionName = "facturas"
    
    func createFactura(_ factura: Factura, callback: @escaping (Bool, Error?) -> Void) {
        let db = Firestore.firestore()
        let facturaData = facturaToDict(factura)
        
        print("Creating new Factura")
        db.collection(self.collectionName).addDocument(data: facturaData) { err in
            if err != nil {
                print("There was an error when creating the Factura. Err = \(err))")
                callback(false, err)
            } else {
                callback(true, nil)
            }
        }
    }
    
    func facturaToDict(_ factura: Factura) -> [String:Any] {
        var dict = [String:Any]()
        dict["valor"] = factura.valor
        dict["fecha"] = factura.fecha
        dict["establecimiento"] = factura.establecimiento
        dict["userId"] = factura.userId
        
        return dict
    }
}

//
//  Parqueadero.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit

class Parqueadero: Codable {
    private var _id: String
    var id: String {
        get { return _id }
        set(val) { _id = val }
    }
    
    private var _nombre: String
    var nombre: String {
        get { return _nombre }
        set(val) { _nombre = val }
    }
    
    private var _numParqueaderos: Int
    var numParqueaderos: Int {
        get { return _numParqueaderos }
        set(val) { _numParqueaderos = val }
    }
    
    private var _disponibles: Int
    var disponibles: Int {
        get { return _disponibles }
        set(val) { _disponibles = val }
    }
    
    private var _direccion: String
    var direccion: String {
        get { return _direccion }
        set(val) { _direccion = val }
    }
    
    private var _nit: String
    var nit: String {
        get { return _nit }
        set(val) { _nit = val }
    }
    
    private var _telefono: String
    var telefono: String {
        get { return _telefono }
    }
    
    private var _imageTag: String
    var imageTag: String {
        get { return _imageTag }
    }
    
    private var _lat: Double
    var lat: Double {
        get { return _lat }
    }
    
    private var _long: Double
    var long: Double {
        get { return _long }
    }
    
    init(id: String, nombre: String, numParqueaderos: Int, disponibles: Int, direccion: String, nit: String, telefono: String, imageTag: String, lat: Double, long: Double) {
        self._id = id
        self._nombre = nombre
        self._numParqueaderos = numParqueaderos
        self._disponibles = disponibles
        self._direccion = direccion
        self._nit = nit
        self._telefono = telefono
        self._imageTag = imageTag
        self._lat = lat
        self._long = long
    }
}

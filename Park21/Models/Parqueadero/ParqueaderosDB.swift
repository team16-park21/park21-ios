//
//  ParqueaderosDB.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Firebase

class ParqueaderosDB {
    static let BASE_URL = "gs://park-21.appspot.com/"
    static let TABLE_QUALITY = 128
    static let TRANSACTION_QUALITY = 1024
    
    func getAllParkingLots(callback: @escaping ([Parqueadero]) -> Void) -> Void {
        let db = Firestore.firestore()
        
        print("Getting all parking lots")
        db.collection("parqueaderos").getDocuments() { (querySnapshot, err) in
            if err != nil {
                print("Error when searching for all parking lots")
                print("Error: \(err!)")
            } else {
                print("Fetch for parqueaderos was OKAY")
                var docsArray: [Parqueadero] = []
                for info in querySnapshot!.documents {
                    let doc = info.data()
                    let entity = Parqueadero(id: info.documentID,
                                             nombre: doc["nombre"] as! String,
                                             numParqueaderos: doc["numeroParqueaderos"] as! Int,
                                             disponibles: doc["disponibles"] as! Int,
                                             direccion: doc["direccion"] as! String,
                                             nit: doc["nit"] as! String,
                                             telefono: doc["telefono"] as! String,
                                             imageTag: doc["imageTag"] as! String,
                                             lat: doc["lat"] as! Double,
                                             long: doc["long"] as! Double)
                    docsArray.append(entity)
                    callback(docsArray)
                }
            }
        }
    }
    
    func createObserverToParqueaderos(onSnapshotCallback: @escaping ([Parqueadero]) -> Void) -> ListenerRegistration {
        let db = Firestore.firestore()
        
        let observer = db.collection("parqueaderos").addSnapshotListener { snapshotBlock, error in
            
            guard let data = snapshotBlock else { return }
            var docsArray: [Parqueadero] = []
            for info in data.documents {
                let doc = info.data()
                let entity = Parqueadero(id: info.documentID,
                                         nombre: doc["nombre"] as! String,
                                         numParqueaderos: doc["numeroParqueaderos"] as! Int,
                                         disponibles: doc["disponibles"] as! Int,
                                         direccion: doc["direccion"] as! String,
                                         nit: doc["nit"] as! String,
                                         telefono: doc["telefono"] as! String,
                                         imageTag: doc["imageTag"] as! String,
                                         lat: doc["lat"] as! Double,
                                         long: doc["long"] as! Double)
                docsArray.append(entity)
            }
            onSnapshotCallback(docsArray)
        }
        
        return observer
    }
    
    func getImageForParqueadero(parqueadero: Parqueadero, quality: Int, callback: @escaping (UIImage) -> Void) {
        let storage = Storage.storage()
        print("Getting image for \(parqueadero.nombre) with URL \(parqueadero.imageTag)")
        let composedURL = "\(ParqueaderosDB.BASE_URL)thumb@\(quality)_\(parqueadero.imageTag)"
        let gsRef = storage.reference(forURL: composedURL)
        
        let size = (quality/2) * 1024
        gsRef.getData(maxSize: Int64(size)) { data, error in
            if error != nil {
                print("Error while fetching image \(error!)")
            } else {
                let image = UIImage(data: data!)!
                ImageCacheManager.storeImage(imageId: parqueadero.id, image: image)
                callback(image)
            }
        }
    }
    
    func getImageForParqueaderoCache(parqueaderoId: String, callback: @escaping (UIImage) -> Void) {
        if ImageCacheManager.isImageInCache(imageId: parqueaderoId) {
            ImageCacheManager.getImage(imageId: parqueaderoId, callback: {img in
                callback(img)
            })
        } else {
            callback(self.getDefaultParqueaderoImage())
        }
    }
    
    func getDefaultParqueaderoImage() -> UIImage {
        return UIImage(named: "defaultParqueadero.jpeg")!
    }
}

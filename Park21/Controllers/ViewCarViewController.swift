//
//  ViewCarViewController.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit
class ViewCarViewController: UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onCallSupportClicked(_ sender: Any) {
        if let securityPhone = AppState.getState().currentParqueadero?.telefono {
            let url = URL(string:"telprompt://\(securityPhone)")
            UIApplication.shared.open(url!)
        } else {
            let url = URL(string:"telprompt://\(AppState.supportNumer)")
            UIApplication.shared.open(url!)
        }
    }
}

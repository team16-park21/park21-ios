//
//  ParqueaderoCell.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/8/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import UIKit

class ParqueaderoCell: UITableViewCell {
    @IBOutlet weak var parqueaderoImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var cuposLabel: UILabel!
    @IBOutlet weak var direccionLabel: UILabel!
    
    func update(_ parqueadero: Parqueadero) {
        self.setName(parqueadero.nombre)
        self.setCupos(parqueadero.disponibles)
        self.setDireccion(parqueadero.direccion)
        ParqueaderosDB().getImageForParqueaderoCache(parqueaderoId: parqueadero.id, callback: {img in
            self.setImage(img)
        })
    }
    
    func setName(_ name: String) {
        nameLabel.text = name
    }
    
    func setCupos(_ cupos: Int) {
        cuposLabel.text = "Cupos: \(cupos)"
    }
    
    func setDireccion(_ dir: String) {
        direccionLabel.text = dir
    }
    
    func setImage(_ image: UIImage) {
        parqueaderoImage.image = image
    }
}

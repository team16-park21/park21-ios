//
//  MainPageViewController.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/5/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Firebase
import UIKit
import MapKit
import CoreLocation
import Network

class MainPageViewController: UIViewController {
    @IBOutlet weak var tableViewParqueaderos: UITableView!
    @IBOutlet weak var mapViewParqueaderos: MKMapView!
    @IBOutlet weak var ProfileButton: UIBarButtonItem!
    
    //Location stuff
    var locationManager: CLLocationManager? = nil
    let MAX_LATITUDE = 5000
    let MAX_LONGITUDE = 5000
    var fixedLocation = false
    
    //Data monitoring
    var networkMonitor: NWPathMonitor? = nil
    var userTransactionObserver: UserTransactionObserver? = nil
    var parqueaderosObserver: ParqueaderosObserver? = nil
    
    let noNetworkConnection = "Has perdido la conexión a internet."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Table View setup
        tableViewParqueaderos.delegate = self
        tableViewParqueaderos.dataSource = self
        
        //Map View setup
        setupMapView()
        
        //Nav bar stuff
        setNavBar()
        
        //DUMP STATE
        AppState.getState().dumpState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        setupObserverToParqueaderos()
        
        //Update user data
        self.manageUserData()
        
        setupNetworkMonitor()
        
        //Unfix map
        setupLocationServices()
        fixedLocation = false
        
        //DUMP STATE
        AppState.getState().dumpState()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        clearNetworkMonitor()
        dismissObservers()
        locationManager = nil
    }
    
    func setupNetworkMonitor() {
        self.networkMonitor = NWPathMonitor()
        networkMonitor?.pathUpdateHandler = { path in
            print("UPDATE FOR NETWORK MONITOR = \(path.status)")
            if path.status != .satisfied {
                self.userTransactionObserver?.dismiss()
                Alerts.showConnectionErrorAlert(presenter: self, message: self.noNetworkConnection)
            } else {
                self.setupObserverForTransactionToUser()
            }
        }
        
        networkMonitor?.start(queue: DispatchQueue.main)
    }
    
    func clearNetworkMonitor() {
        self.networkMonitor?.cancel()
        self.networkMonitor = nil
    }
    
    func dismissObservers() {
        userTransactionObserver?.dismiss()
        userTransactionObserver = nil
        parqueaderosObserver?.dismiss()
        parqueaderosObserver = nil
    }
    
    func setupObserverToParqueaderos() {
        DispatchQueue.global().async {
            let onDismiss: () -> Void = { self.parqueaderosObserver = nil }
            self.parqueaderosObserver = ParqueaderosObserver(onDismissCallback: onDismiss,
                                                        onUpdateCallback: { data in
                AppState.getState().parqueaderos = data
                
                var counter = 0
                let instance = ParqueaderosDB()
                //Fetch images for parqueaderos
                for parqueadero in data {
                    instance.getImageForParqueadero(parqueadero: parqueadero, quality: ParqueaderosDB.TABLE_QUALITY, callback: { _ in
                        counter = counter + 1
                        if (counter == data.count) {
                            DispatchQueue.main.async {
                                self.tableViewParqueaderos.reloadData()
                            }
                        }
                    })
                }
            })
        }
    }
    
    func manageUserData() {
        guard let usr = AppState.getState().usuario else { return }
        UsuariosDB().getUserWithID(usr.id, callback: { data in
            if let updatedUsr = data {
                AppState.getState().usuario = updatedUsr
                //Notify user if he doesn't have a car
                self.checkForCars()
            }
        })
    }
    
    func checkForCars() {
        guard let usr = AppState.getState().usuario else { return }
        if usr.carros.count == 0 {
            Alerts.showNoCarsAlert(presenter: self, handler: { action in
                self.performSegue(withIdentifier: "MainViewToCreateCar", sender: self)
            })
        }
    }
    
    func setupObserverForTransactionToUser() {
        userTransactionObserver = UserTransactionObserver(onDismissCallback: {
            self.userTransactionObserver = nil
        })
    }
    
    func setupMapView() {
        mapViewParqueaderos.delegate = self
        loadMapData()
    }
    
    func setupLocationManager() {
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
    }
    
    func centerMapViewToUserLocation() {
        if !self.fixedLocation {
            guard let location = locationManager?.location?.coordinate else { return }
            let region = MKCoordinateRegion.init(center: location, latitudinalMeters: CLLocationDistance.init(self.MAX_LATITUDE), longitudinalMeters: CLLocationDistance.init(self.MAX_LONGITUDE))
            mapViewParqueaderos.setRegion(region, animated: true)
        }
    }
    
    func setupUserLocationTracking() {
        mapViewParqueaderos.showsUserLocation = true
        centerMapViewToUserLocation()
        locationManager?.startUpdatingLocation()
    }
    
    func checkLocationAccess() {
        switch CLLocationManager.authorizationStatus() {
        case .authorizedWhenInUse:
            setupUserLocationTracking()
            break;
        case .denied:
            showLocationAccessDeniedAlert()
            break;
        case .notDetermined:
            locationManager?.requestWhenInUseAuthorization()
            break;
        case .restricted:
            showLocationAccessDeniedAlert()
            break;
        case .authorizedAlways:
            setupUserLocationTracking()
            break;
        @unknown default:
            showLocationAccessDeniedAlert()
        }
    }
    
    func showLocationAccessDeniedAlert() {
        
    }
    
    func setupLocationServices() {
        self.locationManager = CLLocationManager()
        if CLLocationManager.locationServicesEnabled() {
            setupLocationManager()
            checkLocationAccess()
        } else {
            
        }
    }
    
    func loadMapData() {
        if let parqueaderos = AppState.getState().parqueaderos {
            for parqueadero in parqueaderos {
                let annotation = ParqueaderoMapPoint(parqueaderoName: parqueadero.nombre,
                                                     parqueaderoDireccion: parqueadero.direccion,
                                                     coordinate: CLLocationCoordinate2D(latitude: parqueadero.lat, longitude: parqueadero.long))
                mapViewParqueaderos.addAnnotation(annotation)
            }
        }
    }
    
    func setNavBar() {
        ButtonFormatting.setNavigationBarGreen(self.navigationController!)
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
        navigationItem.setHidesBackButton(true, animated: true)
    }
    
    @IBAction func onProfileButtonClicked(_ sender: Any) {
        self.performSegue(withIdentifier: "MainPageToProfile", sender: self)
    }
}

extension MainPageViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let parqueaderos = AppState.getState().parqueaderos {
            return parqueaderos.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let parqueaderoCell = tableView.dequeueReusableCell(withIdentifier: "ParqueaderoCell", for: indexPath) as! ParqueaderoCell
        
        let parqueadero = AppState.getState().parqueaderos![indexPath.row]
        parqueaderoCell.update(parqueadero)
        
        return parqueaderoCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let long = AppState.getState().parqueaderos?[indexPath.row].long else { return }
        guard let lat = AppState.getState().parqueaderos?[indexPath.row].lat else { return }
        
        self.fixedLocation = true
        let location = CLLocationCoordinate2D(latitude: CLLocationDegrees.init(lat), longitude: CLLocationDegrees.init(long))
        let region = MKCoordinateRegion.init(center: location, latitudinalMeters: CLLocationDistance.init(self.MAX_LATITUDE), longitudinalMeters: CLLocationDistance.init(self.MAX_LONGITUDE))
        
        mapViewParqueaderos.setRegion(region, animated: true)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let handlerFunction: (UIContextualAction, UIView, @escaping (Bool) -> Void) -> Void = {action, view, hizoSegue in
            self.performSegue(withIdentifier: "MainPageToParkingQR", sender: self)
            hizoSegue(true)
        }
        let registerAction = UIContextualAction(style: .normal, title: "Parquear", handler: handlerFunction)
        registerAction.backgroundColor =  #colorLiteral(red: 0.3667953312, green: 0.7296931148, blue: 0.7510977983, alpha: 1)
        return UISwipeActionsConfiguration(actions: [registerAction])
    }
}

extension MainPageViewController: MKMapViewDelegate, CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        if fixedLocation { return }
        
        let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion.init(center: center, latitudinalMeters: CLLocationDistance.init(self.MAX_LATITUDE), longitudinalMeters: CLLocationDistance.init(self.MAX_LONGITUDE))
        mapViewParqueaderos.setRegion(region, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        checkLocationAccess()
    }
}

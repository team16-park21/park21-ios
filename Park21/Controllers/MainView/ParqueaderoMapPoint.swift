//
//  ParqueaderoMapPoint.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/9/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import MapKit

class ParqueaderoMapPoint: NSObject, MKAnnotation {
    let coordinate: CLLocationCoordinate2D
    let title: String?
    let subtitle: String?
    
    init(parqueaderoName: String, parqueaderoDireccion: String, coordinate: CLLocationCoordinate2D) {
        self.title = parqueaderoName
        self.subtitle = parqueaderoDireccion
        self.coordinate = coordinate
    }
}

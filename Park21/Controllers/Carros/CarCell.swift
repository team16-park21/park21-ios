//
//  CarCell.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/11/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit

class CarCell: UITableViewCell {
    @IBOutlet weak var carPlate: UILabel!
    
    func setCarPlate(plate: String) {
        carPlate.text = plate
    }
}

//
//  DetailCarView.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/11/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit

class DetailCarView: UIViewController {
    @IBOutlet weak var carPlate: UILabel!
    @IBOutlet weak var carBrand: UILabel!
    @IBOutlet weak var carModel: UILabel!
    
    @IBOutlet weak var editCar: UIButton!
    
    var carData: Carro? = nil
    
    override func viewDidLoad() {
        carData = AppState.getState().selectedCar
        ButtonFormatting.greenBorderButton(editCar)
        setCarInfo()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        carData = AppState.getState().selectedCar
        setCarInfo()
    }
    
    func setCarInfo() {
        guard let data = carData else { return }
        carPlate.text = "Placa: \(data.placa)"
        carBrand.text = "Marca: \(data.marca)"
        carModel.text = "Modelo: \(data.modelo)"
    }
    
    @IBAction func onEditClicked(_ sender: Any) {
        if AppState.getState().networkStatus {
            self.performSegue(withIdentifier: "EditCar", sender: self)
        } else {
            Alerts.showErrorAlert("No podemos editar el carro en el momento porque no hay conexión a internet. Por favor inténtalo más tarde.", presenter: self)
        }
    }
}

//
//  EditCarView.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/12/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit

class EditCarView: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var plateField: UITextField!
    @IBOutlet weak var modelField: UITextField!
    @IBOutlet weak var brandField: UITextField!
    
    @IBOutlet weak var guardarButton: UIButton!
    
    let noInternetMessage = "No podemos actualizar la información del carro ya que no hay conexión a internet y necesitamos validar la placa de cada carro. Por favor intentalo más tarde."
    
    private var originalCar: Carro? = nil
    
    let defaultAlertMessage = "Ningun campo puede estar vacio."
    
    override func viewDidLoad() {
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
        ButtonFormatting.greenBorderButton(guardarButton)
        setupCarData()
        setupKeyboard()
    }
    
    func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    func setupCarData() {
        if let data = AppState.getState().selectedCar {
            originalCar = data
            let carCopy = Carro(marca: originalCar!.marca, modelo: originalCar!.modelo, placa: originalCar!.placa)
            AppState.getState().editingCarData = carCopy
            showCarData()
        }
    }
    
    func showCarData() {
        plateField.text = originalCar?.placa
        modelField.text = originalCar?.modelo
        brandField.text = originalCar?.marca
    }
    
    @IBAction func plateFieldDidEndEditing(_ sender: Any) {
        AppState.getState().editingCarData?.placa = plateField.text!
    }
    
    @IBAction func modelFieldDidEndEditing(_ sender: Any) {
        AppState.getState().editingCarData?.modelo = modelField.text!
    }
    
    @IBAction func brandFieldDidEndEditing(_ sender: Any) {
        AppState.getState().editingCarData?.marca = brandField.text!
    }
    
    
    @IBAction func onSaveClicked(_ sender: Any) {
        if AppState.getState().networkStatus{
            performEditCar()
        } else {
            Alerts.showConnectionErrorAlert(presenter: self, message: self.noInternetMessage)
        }
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField
        {
           nextField.becomeFirstResponder()
        }
        else
        {
           textField.resignFirstResponder()
        }
        return false
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
              view.endEditing(true)
          }

}

//Edit car flow
extension EditCarView {
    func performEditCar() {
        guard let currentUser = AppState.getState().usuario else { return }
        let carroValidator = CarroValidator()
        
        let plate = plateField.text ?? ""
        let plateValidation = carroValidator.isPlateValid(plate)
        if !plateValidation.isValid {
            Alerts.showErrorAlert(plateValidation.message, presenter: self)
            return
        }
        
        let model = modelField.text ?? ""
        let carroValidation = carroValidator.isModelValid(model)
        if !carroValidation.isValid {
            Alerts.showErrorAlert(carroValidation.message, presenter: self)
            return
        }
        
        let brand = brandField.text ?? ""
        let brandValidation = carroValidator.isBrandValid(brand)
        if !brandValidation.isValid {
            Alerts.showErrorAlert(brandValidation.message, presenter: self)
            return
        }
        
        let newCar = Carro(marca: brand, modelo: model, placa: plate)
        
        if AppState.getState().networkStatus {
            UsuariosDB().editCarFromUser(currentUser, originalCar: originalCar!, editedCar: newCar, callback: {successful, err in
                if successful {
                    AppState.getState().selectedCar = newCar
                    _ = self.navigationController?.popViewController(animated: true)
                } else {
                    Alerts.showErrorAlert("No se pudo editar el carro, intente mas tarde.", presenter: self)
                    print("Could not edit car. \(err!)")
                }
            })
        } else {
            Alerts.showConnectionErrorAlert(presenter: self, message: self.noInternetMessage)
        }
    }
}

//
//  CarLisViewController.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit
class CarListViewController: UIViewController{
    @IBOutlet weak var tableViewCarros: UITableView!
    
    private var carsData: [Carro]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
        setupTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        updateTableViewData()
    }
    
    func setupTableView() {
        tableViewCarros.delegate = self
        tableViewCarros.dataSource = self
        displayCarsData()
        updateTableViewData()
    }
    
    func displayCarsData() {
        if let usr = AppState.getState().usuario {
            carsData = usr.carros
            tableViewCarros.reloadData()
        } else {
            print("user data is empty")
        }
    }
    
    @IBAction func onAddCarClicked(_ sender: Any) {
        if AppState.getState().networkStatus{ self.performSegue(withIdentifier: "AddCarSegue", sender: self)
        }
        else{
            Alerts.showConnectionErrorAlert(presenter: self, message: "No podemos crear el carro porque no hay conexión a internet. Por favor intentalo más tarde.")
        }
    }
}

extension CarListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let data = carsData {
            return data.count
        } else {
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let carCell = tableView.dequeueReusableCell(withIdentifier: "SimpleCarView", for: indexPath) as! CarCell
        
        carCell.setCarPlate(plate: (carsData?[indexPath.row].placa)!)
        
        return carCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let data = carsData?[indexPath.row] {
            AppState.getState().selectedCar = data
            self.performSegue(withIdentifier: "DetailCarViewSegue", sender: self)
        }
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete", handler: {(action, sourceView, completionHandler) in
            guard let carToDelete = self.carsData?[indexPath.row] else {
                completionHandler(true)
                return
            }
            guard let currentUser = AppState.getState().usuario else {
               completionHandler(true)
               return
            }
            
            
            if AppState.getState().networkStatus {
                UsuariosDB().deleteCarFromUser(currentUser, carToDelete: carToDelete, callback: {successful, err in
                    if successful {
                        completionHandler(true)
                        self.updateTableViewData()
                    } else {
                        completionHandler(true)
                    }
                })
            } else {
                Alerts.showErrorAlert("No podemos borrar el carro porque no hay conexión a internet. Por favor intentalo más tarde.", presenter: self)
            }
        })
        
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    func updateTableViewData() {
        guard let usr = AppState.getState().usuario else { return }
        UsuariosDB().getUserWithEmail(usr.correo, callback: {data in
            if let newUsr = data {
                AppState.getState().usuario = newUsr
                self.carsData = newUsr.carros
                self.tableViewCarros.reloadData()
            } else {
                print("Fetch for user was unavailable")
            }
        })
    }
}

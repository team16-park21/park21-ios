//
//  AddCarView.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/12/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit

class AddCarView: UIViewController, UITextFieldDelegate {
    let onCreatedMessage = "El carro ha sido creado exitosamente."
    
    @IBOutlet weak var plateField: UITextField!
    @IBOutlet weak var modelField: UITextField!
    @IBOutlet weak var brandField: UITextField!
    
    @IBOutlet weak var guardarButton: UIButton!
    
    let defaultAlertMessage = "Ningun campo puede estar vacio."
    let noInternetMessage = "No podemos editar el carro ya que no hay conexión a internet y necesitamos validar la placa de cada carro. Por favor intentalo más tarde."
    let couldNotAddCar = "No se pudo agregar el carro a la lista."
    
    override func viewDidLoad() {
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
        ButtonFormatting.greenBorderButton(guardarButton)
    }
    
    @IBAction func onSaveClicked(_ sender: Any) {
        if AppState.getState().networkStatus{
            performAddCar()
        } else {
            Alerts.showConnectionErrorAlert(presenter: self, message: self.noInternetMessage)
        }
        
    }
    
    func performAddCar() {
        let carroValidator = CarroValidator()
        
        //Plate validator
        let plate = plateField.text ?? ""
        let plateValidation = carroValidator.isPlateValid(plate)
        if !plateValidation.isValid {
            Alerts.showErrorAlert(plateValidation.message, presenter: self)
            return
        }
        
        let model = modelField.text ?? ""
        let carroValidation = carroValidator.isModelValid(model)
        if !carroValidation.isValid {
            Alerts.showErrorAlert(carroValidation.message, presenter: self)
            return
        }
        
        let brand = brandField.text ?? ""
        let brandValidation = carroValidator.isBrandValid(brand)
        if !brandValidation.isValid {
            Alerts.showErrorAlert(brandValidation.message, presenter: self)
            return
        }
        
        let carToAdd = Carro(marca: brand, modelo: model, placa: plate)
        guard let currentUser = AppState.getState().usuario else { return }
        UsuariosDB().addCarToUser(currentUser, carToAdd: carToAdd, callback: {successful, err in
            if successful {
                Alerts.showOperationSuccessful(presenter: self, message: self.onCreatedMessage, handler: { action in
                  _ = self.navigationController?.popViewController(animated: true)
                })
            } else {
                print("Could not add new car. \(err!)")
                Alerts.showErrorAlert(self.couldNotAddCar, presenter: self)
            }
        })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField
        {
           nextField.becomeFirstResponder()
        }
        else
        {
           textField.resignFirstResponder()
        }
        return false
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)}
}

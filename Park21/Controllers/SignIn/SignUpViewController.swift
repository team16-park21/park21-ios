//
//  SignUpViewController.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/12/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class SignUpViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    let tryAgainLaterMessage = "No es posible crear un usuario en el momento, por favor inténtelo más tarde."
    
    @IBOutlet weak var NameTxtField: UITextField!
    @IBOutlet weak var passwordTxtField: UITextField!
    @IBOutlet weak var GenderTxtField: UITextField!
    @IBOutlet weak var AgeTxtField: UITextField!
    @IBOutlet weak var IdTxtField: UITextField!
    @IBOutlet weak var mailTxtField: UITextField!
    @IBOutlet weak var phoneTxtField: UITextField!
    @IBOutlet weak var SaveButton: UIButton!
    
    let genders = ["---","Femenino","Masculino","Otro", "Prefiero no decir"]
    
    //Mensajes de alertas
    let noInternetCreateAccountMessage = "No podemos crear tu cuenta porque no hay conexión a internet. Por favor inténtalo más tarde."
    let couldNotCastAgeToInt = "La edad no es válida."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let genderOptions = UIPickerView()
        genderOptions.delegate = self
        
        NameTxtField.delegate = self
        NameTxtField.tag = 0
        passwordTxtField.delegate = self
        passwordTxtField.tag = 1
        GenderTxtField.delegate = self
        GenderTxtField.tag = 2
        GenderTxtField.inputView = genderOptions
        AgeTxtField.delegate = self
        AgeTxtField.tag = 3
        IdTxtField.delegate = self
        IdTxtField.tag = 4
        mailTxtField.delegate = self
        mailTxtField.tag = 5
        phoneTxtField.delegate = self
        phoneTxtField.tag = 6
        
        setUpFormat()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setSignUpButtonStatus(true)
    }
    
    func setSignUpButtonStatus(_ status: Bool) {
        SaveButton.isEnabled = status
        if(status) {
            ButtonFormatting.greenBorderButton(SaveButton)
        } else {
            ButtonFormatting.grayBorderButton(SaveButton)
        }
    }
    
    func setUpFormat()
    {
        navigationController?.setNavigationBarHidden(false, animated: true)
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
        ButtonFormatting.setNavigationBarGreen(self.navigationController!)
        ButtonFormatting.greenBorderTxtField(NameTxtField)
        ButtonFormatting.greenBorderTxtField(passwordTxtField)
        ButtonFormatting.greenBorderTxtField(GenderTxtField)
        ButtonFormatting.greenBorderTxtField(IdTxtField)
        ButtonFormatting.greenBorderTxtField(mailTxtField)
        ButtonFormatting.greenBorderTxtField(phoneTxtField)
        ButtonFormatting.greenBorderTxtField(AgeTxtField)
        ButtonFormatting.greenBorderButton(SaveButton)
    }
    
    @IBAction func onSaveUserInfo(_ sender: Any) {
        setSignUpButtonStatus(false)
        let UserValidator = UsuarioValidator()
        
        if AppState.getState().networkStatus {
            
            // name validator
            let name = NameTxtField.text ?? ""
            let nameValidation = UserValidator.isNameValid(name)
            if !nameValidation.isValid {
                Alerts.showErrorAlert(nameValidation.message, presenter: self)
                self.setSignUpButtonStatus(true)
                return
            }
            
            //password validator
            let password = passwordTxtField.text ?? ""
            let passwordValidation = UserValidator.isPasswordValid(password)
            if !passwordValidation.isValid {
                Alerts.showErrorAlert(passwordValidation.message, presenter: self)
                self.setSignUpButtonStatus(true)
                return
            }
            
            //gender validator
            let gender = GenderTxtField.text ?? ""
            let genderValidation = UserValidator.isGenderValid(gender)
            if !genderValidation.isValid {
                Alerts.showErrorAlert(genderValidation.message, presenter: self)
                self.setSignUpButtonStatus(true)
                return
            }
            
            //age validator
            let ageS = AgeTxtField.text ?? ""
            let ageValidation = UserValidator.isAgeValid(ageS)
            if !ageValidation.isValid {
                Alerts.showErrorAlert(ageValidation.message, presenter: self)
                self.setSignUpButtonStatus(true)
                return
            }
            
            //Cast age string to int
            guard let age:Int = Int(ageS) else {
                Alerts.showErrorAlert(self.couldNotCastAgeToInt, presenter: self)
                self.setSignUpButtonStatus(true)
                return
            }
            
            //Cedula validator
            let cedula = IdTxtField.text ?? ""
            let cedulaValidation = UserValidator.isCedulaValid(cedula)
            if !cedulaValidation.isValid {
                Alerts.showErrorAlert(cedulaValidation.message, presenter: self)
                self.setSignUpButtonStatus(true)
                return
            }
            
            //email validator
            let email = mailTxtField.text ?? ""
            let emailValidation = UserValidator.isMailValid(email)
            if !(emailValidation.isValid) {
                Alerts.showErrorAlert(emailValidation.message, presenter: self)
                self.setSignUpButtonStatus(true)
                return
            }
            
            //phone validator
            let phone = phoneTxtField.text ?? ""
            let phoneValidation = UserValidator.isPhoneValid(phone)
            if !phoneValidation.isValid {
                Alerts.showErrorAlert(phoneValidation.message, presenter: self)
                self.setSignUpButtonStatus(true)
                return
            }
            
            let userToAdd = Usuario(id: "",
                                    correo: email,
                                    cedula: cedula,
                                    nombre: name,
                                    telefono: phone,
                                    genero: gender,
                                    edad: age,
                                    carData: [],
                                    facturas: [],
                                    transaccionActual: "",
                                    familyGroupId: "")
            
            createUserInDB(userToAdd, password)
        }
        else {
            Alerts.showConnectionErrorAlert(presenter: self, message: self.noInternetCreateAccountMessage)
            setSignUpButtonStatus(true)
        }
    }
    
    func createUserInDB(_ userToAdd: Usuario, _ password: String) {
        if AppState.getState().networkStatus {
            UsuariosDB().createUser(userToAdd, callback: { successful, err in
                if successful {
                    Auth.auth().createUser(withEmail: userToAdd.correo, password: password) { authResult, error in
                        if error == nil {
                            Alerts.showMessageAlertWithHandler(
                                "El usuario fue creado correctamente. Ya puede iniciar sesión.",
                                presenter: self,
                                handler: {action in
                                    _ = self.navigationController?.popViewController(animated: true)
                            })
                        } else {
                            print("ERROR CREATING USER INSIDE FIREBASE == \(error)")
                            Alerts.showErrorAlert(self.tryAgainLaterMessage, presenter: self)
                            self.setSignUpButtonStatus(true)
                        }
                    }
                } else {
                    if err != nil {
                        Alerts.showErrorAlert(self.tryAgainLaterMessage, presenter: self)
                        self.setSignUpButtonStatus(true)
                    } else {
                        Alerts.showErrorAlert("El correo usado ya existe en la base de datos. Por favor use otro correo.", presenter: self)
                        self.setSignUpButtonStatus(true)
                    }
                }
            })
        } else {
            Alerts.showConnectionErrorAlert(presenter: self, message: "No podemos crear a tu cuenta porque no hay conexión a internet. Por favor inténtalo más tarde.")
            self.setSignUpButtonStatus(true)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField
        {
            nextField.becomeFirstResponder()
        }
        else
        {
            textField.resignFirstResponder()
        }
        return false
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= 100
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genders.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        GenderTxtField.text = genders[row]
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
         var maxLength = 10
         if textField == NameTxtField
         {
             maxLength = 65
         }
         else if textField == AgeTxtField
         {
             maxLength = 3
         }
         else if textField == phoneTxtField
         {
             maxLength = 15
         }
         else if textField == IdTxtField
         {
             maxLength = 15
         }
         else if textField == mailTxtField
         {
             maxLength = 65
         }
        else if textField == passwordTxtField
         {
            maxLength = 50
        }
         // get the current text, or use an empty string if that failed
         let currentText = textField.text ?? ""

         // attempt to read the range they are trying to change, or exit if we can't
         guard let stringRange = Range(range, in: currentText) else { return false }

         // add their new text to the existing text
         let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

         // make sure the result is under 16 characters
         return updatedText.count <= maxLength
     }
}

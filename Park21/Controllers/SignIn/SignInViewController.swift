//
//  SignInController.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/5/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController, UITextFieldDelegate {
    let logInErrorMessage = "Usuario ó contraseña incorrectos."
    
    @IBOutlet weak var SignInButton: UIButton!
    @IBOutlet weak var UserTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    var username = ""
    var password = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ButtonFormatting.greenBorderButton(SignInButton)
        ButtonFormatting.greenBorderTxtField(UserTextField)
        ButtonFormatting.greenBorderTxtField(PasswordTextField)
        PasswordTextField.delegate = self
        UserTextField.delegate = self
        UserTextField.tag = 0
        PasswordTextField.tag = 1
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        let nav = self.navigationController?.navigationBar
        let col = #colorLiteral(red: 0.3667953312, green: 0.7296931148, blue: 0.7510977983, alpha: 1)
        nav?.tintColor = col
    }
    
    func getText(_ textField: UITextField) -> String
    {
        if let text = textField.text
        {
            return text
        }
        else { return "Error"}
    }
    
    func setLoginButtonStatus(_ status: Bool) {
        SignInButton.isEnabled = status
        if status {
            ButtonFormatting.greenBorderButton(SignInButton)
        } else {
            ButtonFormatting.grayBorderButton(SignInButton)
        }
    }
    
    @IBAction func signIn(_ sender: UIButton) {
        setLoginButtonStatus(false)
        if AppState.getState().networkStatus
        {
            username = getText(UserTextField)
            password = getText(PasswordTextField)
            
            if username.isEqual("Error") || password.isEqual("Error") || username.isEqual("") || password.isEqual("")
            {
                Alerts.showErrorAlert("Ningun campo puede estar vacio", presenter: self.navigationController!)
                setLoginButtonStatus(true)
                username = ""
                password = ""
            }
            else
            {
                Alerts.showInProgressView(self, message: "Iniciando sesión...", onCompletion: nil)
                handleLogIn(username, password)
            }
        }
        else {
            setLoginButtonStatus(true)
            Alerts.showConnectionErrorAlert(presenter: self, message:"No podemos ingresar a tu cuenta porque no hay conexión a internet. Por favor intentalo más tarde.")
        }
    }
    
    func handleLogIn(_ username: String, _ password: String) {
        Auth.auth().signIn(withEmail: username, password: password) { [weak self] authResult, error in
            guard let strongSelf = self else {
                self?.setLoginButtonStatus(true)
                return
            }
            
            if error == nil {
                //Get usuarios DB and change state to new data
                let db = UsuariosDB()
                db.getUserWithEmail(username, callback: {usr in
                    if let usr = usr {
                        AppState.getState().usuario = usr
                        strongSelf.decideWichViewToPresent(usr)
                    }
                    else {
                        Alerts.stopInProgressView(strongSelf)
                        strongSelf.perform(#selector(strongSelf.showInfoNotValidAlert), with: nil, afterDelay: TimeInterval(exactly: Alerts.DEFAULT_DISMISS_TIME)!)
                        strongSelf.showFailedLoginAlert()
                        strongSelf.setLoginButtonStatus(true)
                    }
                })
            } else {
                strongSelf.setLoginButtonStatus(true)
                Alerts.stopInProgressView(strongSelf)
                strongSelf.perform(#selector(strongSelf.showInfoNotValidAlert), with: nil, afterDelay: TimeInterval(exactly: Alerts.DEFAULT_DISMISS_TIME)!)
                strongSelf.showFailedLoginAlert()
            }
        }
    }
    
    func showFailedLoginAlert() {
        Alerts.showErrorAlert(self.logInErrorMessage, presenter: self)
    }
    
    @objc func showInfoNotValidAlert() {
        Alerts.showErrorAlert("La información ingresada no es válida", presenter: self.navigationController!)
    }
    
    func decideWichViewToPresent(_ user: Usuario) {
        if AppState.getState().networkStatus && user.transaccionActual != "" {
            TransaccionesDB().getTransaccionWithID(user.transaccionActual, callback: {data in
                if let transaction = data {
                    AppState.getState().userInTransaction = true
                    AppState.getState().currentTransaction = transaction
                    Alerts.stopInProgressView(self)
                    self.perform(#selector(self.presentMainParkingView), with: nil, afterDelay: TimeInterval(exactly: Alerts.DEFAULT_DISMISS_TIME)!)
                    self.setLoginButtonStatus(true)
                }  else {
                    Alerts.stopInProgressView(self)
                    self.perform(#selector(self.presentMainPage), with: nil, afterDelay: TimeInterval(exactly: Alerts.DEFAULT_DISMISS_TIME)!)
                    self.setLoginButtonStatus(true)
                }
            })
        } else {
            Alerts.stopInProgressView(self)
            self.perform(#selector(self.presentMainPage), with: nil, afterDelay: TimeInterval(exactly: Alerts.DEFAULT_DISMISS_TIME)!)
            self.setLoginButtonStatus(true)
        }
    }
    
    @objc func presentMainParkingView() {
        self.performSegue(withIdentifier: "ToMainParkingView", sender: self)
    }
    
    @objc func presentMainPage() {
        self.performSegue(withIdentifier: "ToMainPage", sender: self)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField{
            nextField.becomeFirstResponder()
        }
        else {
            // Not found, so remove keyboard.
            textField.resignFirstResponder()
        }
        return false
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength = 10
        if textField == UserTextField
        {
            maxLength = 65
        }
        else if textField == PasswordTextField
        {
            maxLength = 50
        }
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        // make sure the result is under 16 characters
        return updatedText.count <= maxLength
    }
    @objc func keyboardWillShow(notification: NSNotification) {
        if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= 50
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
}

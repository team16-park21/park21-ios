//
//  ViewController.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 9/27/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleButtons()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setButtonsStatus(status: false)
        verifyLoginStatus()
        setNavBarToHidden(status: true, animated: animated)
    }
    
    func setNavBarToHidden(status: Bool, animated: Bool) {
        navigationController?.setNavigationBarHidden(status, animated: animated)
    }
    
    func verifyLoginStatus() {
        if let _ = AppState.getState().usuario {
            AppState.getState().transactionIsPayed = false
            Alerts.showInProgressView(self, message: "Iniciando sesión...", onCompletion: nil)
            if let _ = AppState.getState().currentTransaction {
                Alerts.stopInProgressView(self)
                perform(#selector(presentParkingMainView), with: nil, afterDelay: TimeInterval(exactly: Alerts.DEFAULT_DISMISS_TIME)!)
            } else {
                Alerts.stopInProgressView(self)
                perform(#selector(presentMainView), with: nil, afterDelay: TimeInterval(exactly: Alerts.DEFAULT_DISMISS_TIME)!)
            }
        } else {
            setButtonsStatus(status: true)
        }
    }
    
    @objc func presentMainView() {
        setButtonsStatus(status: true)
        performSegue(withIdentifier: "ToAppMainView", sender: self)
    }
    
    @objc func presentParkingMainView() {
        setButtonsStatus(status: true)
        AppState.getState().userInTransaction = true
        performSegue(withIdentifier: "ToParkingMainView", sender: self)
    }
    
    func setButtonsStatus(status: Bool) {
        signInButton.isEnabled = status
        signUpButton.isEnabled = status
    }
    
    func styleButtons() {
        ButtonFormatting.greenBorderButton(signInButton)
        ButtonFormatting.greenBorderButton(signUpButton)
    }
    
    @IBAction func SignInButton(_ sender: Any) {
        if AppState.getState().networkStatus {
            performSegue(withIdentifier: "toSignInView", sender: self)
        }
        else {
            performSegue(withIdentifier: "WelcomeViewEventualConnectivity", sender: self)
        }
    }
    
    
    @IBAction func SignUpButton(_ sender: Any) {
        if AppState.getState().networkStatus {
            performSegue(withIdentifier: "toSignUpView", sender: self)
        }
        else{
            performSegue(withIdentifier: "WelcomeViewEventualConnectivity", sender: self)
        }
    }
}


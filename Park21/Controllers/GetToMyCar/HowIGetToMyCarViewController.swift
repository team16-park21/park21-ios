//
//  HowIGetToMyCarViewController.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit
class HowIGetToMyCarViewController: UIViewController {
    @IBOutlet weak var instruccionesTable: UITableView!
    var instrucciones: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTable()
        updateData()
        displayData()
        instruccionesTable.rowHeight = UITableView.automaticDimension
        instruccionesTable.estimatedRowHeight = 600
    }
    
    func setupTable() {
        instruccionesTable.delegate = self
        instruccionesTable.dataSource = self
    }
    
    func updateData() {
        guard let transaccion = AppState.getState().currentTransaction else {
            print("ERROR, NO TRANSACTION WAS FOUND FOR CURRENT STATE")
            return
        }
        
        if let data = transaccion.puestoParqueo?.instrucciones {
            self.instrucciones = data.components(separatedBy: ";")
        }
    }
    
    func displayData() {
        self.instruccionesTable.reloadData()
    }
    
    @IBAction func onCallClicked(_ sender: Any) {
        if let securityPhone = AppState.getState().currentParqueadero?.telefono {
                   let url = URL(string:"telprompt://\(securityPhone)")
                   UIApplication.shared.open(url!)
               } else {
                   let url = URL(string:"telprompt://\(AppState.supportNumer)")
                   UIApplication.shared.open(url!)
               }
    }
}

extension HowIGetToMyCarViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "instruccionCell", for: indexPath) as! InstruccionCell
        
        let instruccion = instrucciones[indexPath.row]
//        cell.label.lineBreakMode = NSLineBreakMode.byWordWrapping
        cell.update(instruccion)
    
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return instrucciones.count
    }
}

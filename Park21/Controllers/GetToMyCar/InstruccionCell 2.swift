//
//  InstructionCell.swift
//  Park21
//
//  Created by Maria del Rosario León on 11/12/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit
class InstruccionCell: UITableViewCell {
    @IBOutlet weak var label: UILabel!
    
    func update(_ instruction: String) {
        self.setInstruction(instruction: instruction)
    }
    
    func setInstruction(instruction: String){
        label.text = instruction
    }
}

//
//  FamilyGroupOptions.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/29/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import UIKit
class FamilyGroupOptions: UIViewController{
    
    let joinGroupConectivity = "No podemos añadirte al grupo porque no tienes conexión a internet. Por favor inténtalo más tarde."
    let connectionError = "No podemos crear un grupo familiar porque no hay internet. Por favor inténtalo más tarde."
    
    @IBOutlet weak var createGroup: UIButton!
    @IBOutlet weak var joinGroup: UIButton!
    
    override func viewDidLoad() {
        buttonFormating()
    }
    
    func buttonFormating(){
        ButtonFormatting.greenBorderButton(createGroup)
        ButtonFormatting.greenBorderButton(joinGroup)
    }
    
    @IBAction func onJoinGroupClicked(_ sender: UIButton) {
        if AppState.getState().networkStatus{
            performSegue(withIdentifier: "familyGroupViewToScanCoode", sender: self)
        }
        else {
            Alerts.showConnectionErrorAlert(presenter: self, message: joinGroupConectivity)
        }
    }
    
    @IBAction func onCreateGroup(){
        if AppState.getState().networkStatus {
            if let currentUser = AppState.getState().usuario {
                let currentIntegrante = Familia.Integrante(nombre: currentUser.nombre, id: currentUser.id)
                print("BEFORE CREATE")
                FamiliaDB().createFamilyGroup(integrante: currentIntegrante, callback: { family, id, successful, err in
                    print("AFTER CREATE")
                    if successful {
                        AppState.getState().family = family
                        AppState.getState().family?.id = id!
                        UsuariosDB().editFamilyGroupForUserWithID(currentUser.id, familyId: id!, callback: { successful, err in
                            if successful{
                                currentUser.familyGroupId = id!
                                self.performSegue(withIdentifier: "toGroupFamilyView", sender: self)
                            }
                            else{
                                Alerts.showErrorAlert("No se pudo crear el grupo familiar.", presenter: self)
                            }
                        })
                    } else {
                        //ALERTA
                        print("Error al crear el grupo familiar: \(err)")
                    }
                
            })
            } else {
                print("Error con el usuario")
            }
        } else {
            Alerts.showConnectionErrorAlert(presenter: self, message: connectionError)
        }
    }
    
}

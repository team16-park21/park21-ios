
import UIKit
import AVFoundation

class FamilyGroupScanCode: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!

    override func viewDidLoad() {
        super.viewDidLoad()
        ButtonFormatting.setLogoAsTitle(self.navigationItem)

        view.backgroundColor = UIColor.black
        captureSession = AVCaptureSession()

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput

        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }

        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }

        let metadataOutput = AVCaptureMetadataOutput()

        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)

            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)

        captureSession.startRunning()
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()

        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(familiaId: stringValue)
        }

        dismiss(animated: true)
    }
    
    func found(familiaId: String) {
        if familiaId != "" {
            FamiliaDB().getFamilyWithId(familiaId, callback: { data in
                if data != nil {
                    self.addMember(id: familiaId, currentFamily: data!)
                } else {
                    //Aca se maneja el caso en que el id no es valido, pedir volver a escanear
                }
            })
        }
    }
    
    func addMember(id: String, currentFamily: Familia) {
        guard let currentUser = AppState.getState().usuario else {
            print("ERROR WITH APP STATE, NO USER FOUND")
            return
        }
        
        let integrante = Familia.Integrante(nombre: currentUser.nombre, id: currentUser.id)
        
        FamiliaDB().addMemberToFamilyWithId(idFamily: id, integrante: integrante, currentFamily, callback: { successful, err in
            if successful {
                
                UsuariosDB().editFamilyGroupForUserWithID(currentUser.id, familyId: id, callback: { successful, err in
                    if successful{
                        AppState.getState().usuario!.familyGroupId = id
                        self.performSegue(withIdentifier: "scanQRViewToFamilyGroupListView", sender: self)
                    }
                    else{
                        //ALERT
                        print("There was an error when attempting to add the family id to the user. Err = \(err!)")
                    }
                    
                })
                //Enviar al usuario a su nueva vista de familia. Cambiar el familyId del usuario en Firebase y actualizar local
            } else {
                //ALERT
                print("There was an error when attempting to add a memeber to the family. Err = \(err!)")
            }
        })
    }
    
    override var prefersStatusBarHidden: Bool {
           return false
       }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
}

//
//  FamilyGroupView.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/29/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit
class FamilyGroupView: UIViewController {
    //MARK: Alert's messages
    let conectionErrorWhenAddMemeber = "No podemos agregar un miembro al grupo pues no tienes conexión a internet. Inténtalo más tarde"
    let conectionErrorWhenDeleting = "No podemos eliminar un miembro del grupo pues no tienes conexión a internet. Inténtalo más tarde"
    let conectionErrorWhenExiting = "No puedes abandonar el grupo en el momento pues no tienes conexión a internet. Inténtalo más tarde"
    let abandonarTitle = "Abandonar grupo"
    let abandonarMensaje = "¿Estás seguro que deseas abandonar el grupo familiar?"
    let removedFromGroupMessage = "Ya no haces parte de este grupo familiar."
    
    @IBOutlet weak var addMemberButton: UIBarButtonItem!
    @IBOutlet weak var membersTableView: UITableView!
    
    var currentFamily: Familia? = nil
    var familiaObserver: FamiliaObserver? = nil
    var currentUser: Usuario?
    
    override func viewDidLoad() {
        setUpTableView()
        updateFamilyData()
        navigationController?.deleteInBetweenTopAndClassFromStack(ofClass: ProfileViewController.self, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        currentUser = AppState.getState().usuario
        setupFamiliaObserver()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        dismissFamiliaObserver()
        super.viewWillDisappear(animated)
    }
    
    func setupFamiliaObserver() {
        familiaObserver = FamiliaObserver(onDismissCallback: {
            self.familiaObserver = nil
        }, onUpdateCallback: { familia in
            let usrId = AppState.getState().usuario?.id ?? ""
            var isInFamily = false
            for member in familia.users {
                if member.id == usrId {
                    isInFamily = true
                }
            }
            
            if isInFamily {
                self.currentFamily = familia
                AppState.getState().family = familia
                self.membersTableView.reloadData()
            } else {
                AppState.getState().family = nil
                AppState.getState().usuario?.familyGroupId = ""
                Alerts.showMessageAlertWithHandler(self.removedFromGroupMessage, presenter: self, handler: { _ in
                    _ = self.navigationController?.popViewController(animated: true)
                })
            }
        })
    }
    
    func dismissFamiliaObserver() {
        familiaObserver?.dismiss()
        familiaObserver = nil
    }
    
    func setUpTableView() {
        membersTableView.delegate = self
        membersTableView.dataSource = self
    }
    
    func updateFamilyData() {
        guard let currentUser = AppState.getState().usuario else { return }
        FamiliaDB().getFamilyWithId(currentUser.familyGroupId, callback: { data in
            if let familiaUpdate = data {
                AppState.getState().family = familiaUpdate
                self.currentFamily = familiaUpdate
                self.membersTableView.reloadData()
            }
        })
    }
    
    @IBAction func onAddMemberClicked(_ sender: UIBarButtonItem) {
        if AppState.getState().networkStatus
        {
            performSegue(withIdentifier: "toAddMemberQR", sender: self)
        }
        else{
            Alerts.showConnectionErrorAlert(presenter: self, message: conectionErrorWhenAddMemeber)
        }
    }
}

extension FamilyGroupView: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let familia = currentFamily{
            return familia.users.count
        }
        else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let memberCell = tableView.dequeueReusableCell(withIdentifier: "familyMemberCell", for: indexPath) as! MemberCell
        
        let integrante = currentFamily!.users[indexPath.row]
        
        memberCell.update(integrante)
        
        return memberCell
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let usr = self.currentFamily!.users[indexPath.row]
        let currentId = currentUser?.id ?? ""
        if usr.id == currentId {
            return getSwipeActionsConfigurationForMainUser(indexPath)
        } else {
            return getSwipeActionsConfigurationForOtherUser(indexPath)
        }
    }
    
    func getSwipeActionsConfigurationForOtherUser(_ indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Expulsar", handler: {(action, sourceView, completionHandler) in
            guard let memberToDelete = self.currentFamily?.users[indexPath.row] else {
                completionHandler(true)
                return
            }
            
            if AppState.getState().networkStatus{
                FamiliaDB().deleteMemberWithIDFromFamily(memberToDelete.id, self.currentFamily!, callback: {successful, err in
                    if successful{
                        completionHandler(true)
                        self.updateFamilyData()
                    }
                    else{
                        completionHandler(true)
                    }
                })
            }
            else{
                Alerts.showConnectionErrorAlert(presenter: self, message: self.conectionErrorWhenDeleting)
                completionHandler(false)
            }
        })
        return UISwipeActionsConfiguration(actions: [delete])
    }
    
    func getSwipeActionsConfigurationForMainUser(_ indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Abandonar", handler: {(action, sourceView, completionHandler) in
            guard let memberToDelete = self.currentFamily?.users[indexPath.row] else {
                completionHandler(true)
                return
            }
            
            let handlerYes: (UIAlertAction) -> Void = {action in
                if AppState.getState().networkStatus{
                    FamiliaDB().deleteMemberWithIDFromFamily(memberToDelete.id, self.currentFamily!, callback: {successful, err in
                        if successful {
                            AppState.getState().usuario?.familyGroupId = ""
                            _ = self.navigationController?.popViewController(animated: true)
                            completionHandler(true)
                        }
                        else{
                            completionHandler(true)
                        }
                    })
                }
                else{
                    Alerts.showConnectionErrorAlert(presenter: self, message: self.conectionErrorWhenDeleting)
                    completionHandler(false)
                }
            }
            
            Alerts.showYesNoAlertWithHandlers(title: self.abandonarTitle, message: self.abandonarMensaje, presenter: self, handlerYes: handlerYes, handlerNo: { _ in completionHandler(false)})
        })
        return UISwipeActionsConfiguration(actions: [delete])
    }
}

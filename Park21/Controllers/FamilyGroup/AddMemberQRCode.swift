//
//  AddMemberQRCode.swift
//  Park21
//
//  Created by Maria del Rosario León on 11/7/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit
class AddMemberQRCode: UIViewController {
    
    @IBOutlet weak var qrCodeImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        generateQRCode()
    }
    
    func generateQRCode() {
        //Read from https://medium.com/@dominicfholmes/generating-qr-codes-in-swift-4-b5dacc75727c
        let qrMessage = AppState.getState().usuario!.familyGroupId
        let data = qrMessage.data(using: String.Encoding.ascii)
        guard let qrFilter = CIFilter(name: "CIQRCodeGenerator") else { return }
        qrFilter.setValue(data, forKey: "inputMessage")
        guard let qrImage = qrFilter.outputImage else { return }
        let transform = CGAffineTransform(scaleX: 10, y: 10)
        let scaledQrImage = qrImage.transformed(by: transform)
        
        qrCodeImage.image = UIImage(ciImage: scaledQrImage)
    }
}

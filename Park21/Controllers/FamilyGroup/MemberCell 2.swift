//
//  MemberCell.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/30/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit
class MemberCell: UITableViewCell {
    
    @IBOutlet weak var membersNameLabel: UILabel!
    @IBOutlet weak var membersProfilePic: UIImageView!
    
    func update(_ member: Familia.Integrante)
    {
        self.setName(member.nombre)
        self.setMembersProfilePic()
    }
    
    func setName(_ name: String){
        membersNameLabel.text = name
    }
    
    func setMembersProfilePic ()
    {
        membersProfilePic.image = #imageLiteral(resourceName: "UserIcon")
        
    }
    
}

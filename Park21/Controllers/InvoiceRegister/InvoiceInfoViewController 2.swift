//
//  InvoiceInfoViewController.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class InvoiceInfoViewController: UIViewController {
    //Alertas
    let permisoCameraMessage = "Es necesario que la aplicación tenga acceso a la camara para poder registrar facturas. Por favor habilite el acceso para hacer uso de esta funcionalidad."
    let errorFacturaCreateMessage = "No se pudo registrar la factura a su nombre. Por favor intentelo más tarde."
    let noConnectionErrorMessage = "No hay conexión a internet en el momento. Por favor intentelo más tarde."
    let couldNotCastValorToInt = "El valor no es válido."
    
    @IBOutlet weak var nombreTextField: UITextField!
    @IBOutlet weak var fechaTextField: UITextField!
    @IBOutlet weak var valorTextField: UITextField!
    
    @IBOutlet weak var registrarButton: UIButton!
    @IBOutlet weak var reTakePictureButton: UIButton!
    
    var imagePicker: UIImagePickerController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
        setupButtonsAndTextFields()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        showData()
    }
    
    func setupButtonsAndTextFields() {
        ButtonFormatting.greenBorderButton(registrarButton)
        ButtonFormatting.greenBorderButton(reTakePictureButton)
        
        fechaTextField.isEnabled = false
        nombreTextField.isEnabled = true
        nombreTextField.delegate = self
        valorTextField.isEnabled = true
        valorTextField.delegate = self
    }
    
    func showData() {
        if let currentFactura = AppState.getState().factura {
            nombreTextField.text = currentFactura.establecimiento
            valorTextField.text = String(currentFactura.valor)
            
            let df = DateFormatter()
            df.dateFormat = "dd/MM/yy"
            let dateAsString = df.string(from: currentFactura.fecha)
            fechaTextField.text = dateAsString
        } else {
            
        }
    }
    
    @IBAction func onRegistrarFacturaClicked(_ sender: Any) {
        let facturaValidator = FacturaValidator()

        guard let factura = AppState.getState().factura else {
            print("NO FACTURA FOUND FOR CURRENT STATE")
            return
        }
        
        //Register factura to user
        guard let usr = AppState.getState().usuario else {
            print("NO USER FOUND ON STATE")
            return
        }
        factura.userId = usr.id
        
        //Valor validator
        let inputValor = valorTextField.text ?? ""
        let valorValidation = facturaValidator.isValorValid(inputValor)
        if !valorValidation.isValid {
            Alerts.showErrorAlert(valorValidation.message, presenter: self)
            return
        }
        
        guard let valorInt = Int(inputValor) else {
            Alerts.showErrorAlert(self.couldNotCastValorToInt, presenter: self)
            return
        }
        
        //Establecimiento validator
        let inputEstablecimiento = nombreTextField.text ?? ""
        let establecimientoValidation = facturaValidator.isEstablecimientoValid(inputEstablecimiento)
        if !establecimientoValidation.isValid {
            Alerts.showErrorAlert(establecimientoValidation.message, presenter: self)
            return
        }
        
        //Update with currently displayed data
        factura.valor = valorInt
        factura.establecimiento = inputEstablecimiento
        
        if AppState.getState().networkStatus {
            FacturasDB().createFactura(factura, callback: { successful, err in
                if successful {
                    self.presentPayParkingView()
                } else {
                    Alerts.showErrorAlert(self.errorFacturaCreateMessage, presenter: self)
                }
            })
        } else {
            Alerts.showConnectionErrorAlert(presenter: self, message: self.noConnectionErrorMessage)
        }
    }
    
    func presentPayParkingView() {
        //Replace nav controller stack
        let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let mainParkingView : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "MainParkingView")
        let payParkingView : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "PayParkingView")
        
        if let navVC = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
            navVC.setViewControllers([mainParkingView, payParkingView], animated: true)
        }
    }
}

extension InvoiceInfoViewController: UITextFieldDelegate {
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y == 0 {
                self.view.frame.origin.y -= keyboardSize.height
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
}

extension InvoiceInfoViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBAction func onReTakePictureClicked(_ sender: Any) {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if !granted {
                    Alerts.showNeedsPermissonsAlert(self.permisoCameraMessage, presenter: self)
                }
            })
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let image = info[.originalImage] as? UIImage else { return }
        generateImageData(image, callback: {
            self.imagePicker.dismiss(animated: true, completion: nil)
        })
    }
    
    func generateImageData(_ image: UIImage, callback: @escaping () -> Void) {
        let factory = ImageTextProcessor()
        factory.process(image, callback: { blocks in
            let factura = factory.createFacturaFromBlocks(blocks)
            AppState.getState().factura = factura
            callback()
        })
    }
}

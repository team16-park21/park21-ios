 //
//  SettingsViewController.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit
class SettingsViewController: UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
    }
}

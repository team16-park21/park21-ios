//
//  EditProfileViewController.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit

class EditProfileViewController: UIViewController
{
    //UI TextFields
    @IBOutlet weak var nombreField: UITextField!
    @IBOutlet weak var generoField: UITextField!
    @IBOutlet weak var edadField: UITextField!
    @IBOutlet weak var telefonoField: UITextField!
    @IBOutlet weak var cedulaField: UITextField!
    @IBOutlet weak var correoField: UITextField!
    
    //UI Images
    @IBOutlet weak var profileImage: UIImageView!
    
    //UI Buttons
    @IBOutlet weak var SaveButton: UIButton!
    @IBOutlet weak var CancelButton: UIBarButtonItem!
    
    //Picker View
    let genderOptions = UIPickerView()
    
    //Data
    var originalUserData: Usuario? = nil
    let genders = ["---","Femenino","Masculino","Otro", "Prefiero no decir"]
    
    let noInternetEditAccountMessage = "No podemos editar la información de tu perfil porque no hay conexión a internet. Por favor intentalo más tarde."
    let couldNotCastAgeToInt = "La edad no es válida."
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formatButtons()
        setupDelegates()
        displayUserInfo()
    }
    
    func setupDelegates() {
        nombreField.delegate = self
        nombreField.tag = 0
        
        genderOptions.delegate = self
        generoField.delegate = self
        generoField.tag = 1
        generoField.inputView = genderOptions
        
        edadField.delegate = self
        edadField.tag = 2
        
        telefonoField.delegate = self
        telefonoField.tag = 3
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    func displayUserInfo() {
        guard let data = AppState.getState().usuario else { return }
        self.originalUserData = data
        
        nombreField.text = originalUserData!.nombre
        edadField.text = String(originalUserData!.edad)
        cedulaField.text = originalUserData!.cedula
        telefonoField.text = originalUserData!.telefono
        correoField.text = originalUserData!.correo
        generoField.text = originalUserData!.genero
        
        if originalUserData?.genero == "Femenino" {
            profileImage.image = #imageLiteral(resourceName: "defaultProfileFemale")
        } else {
            profileImage.image = #imageLiteral(resourceName: "defaultProfileMale")
        }
    }
    
    func formatButtons() {
        ButtonFormatting.greenBorderTxtField(nombreField)
        ButtonFormatting.greenBorderTxtField(generoField)
        ButtonFormatting.greenBorderTxtField(edadField)
        ButtonFormatting.greenBorderTxtField(telefonoField)
        ButtonFormatting.greenBorderButton(SaveButton)
        ButtonFormatting.setNavigationBarGreen(self.navigationController!)
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
    }
    
    @IBAction func onSave(_ sender: UIButton) {
        if AppState.getState().networkStatus{
            performEdit()
            print("Edit exitoso")
        }
        else{
            Alerts.showConnectionErrorAlert(presenter: self, message: self.noInternetEditAccountMessage)
        }
    }
    
    func performEdit() {
        guard let currentUsr = AppState.getState().usuario else { return }
        let userValidator = UsuarioValidator()
        
        // name validator
        let name = nombreField.text ?? ""
        let nameValidation = userValidator.isNameValid(name)
        if !nameValidation.isValid {
            Alerts.showErrorAlert(nameValidation.message, presenter: self)
            return
        }
        
        //gender validator
        let gender = generoField.text ?? ""
        let genderValidation = userValidator.isGenderValid(gender)
        if !genderValidation.isValid {
            Alerts.showErrorAlert(genderValidation.message, presenter: self)
            return
        }
        
        //age validator
        let ageS = edadField.text ?? ""
        let ageValidation = userValidator.isAgeValid(ageS)
        if !ageValidation.isValid {
            Alerts.showErrorAlert(ageValidation.message, presenter: self)
            return
        }
        
        //Cast age string to int
        guard let age:Int = Int(ageS) else {
            Alerts.showErrorAlert(self.couldNotCastAgeToInt, presenter: self)
            return
        }
        
        //telefono validator
        let phone = telefonoField.text ?? ""
        let phoneValidation = userValidator.isPhoneValid(phone)
        if !phoneValidation.isValid {
            Alerts.showErrorAlert(phoneValidation.message, presenter: self)
            return
        }
        
        let usuariosDB = UsuariosDB()
        let userToPatch = Usuario(id: currentUsr.id,
                                  correo: currentUsr.correo,
                                  cedula: currentUsr.cedula,
                                  nombre: name,
                                  telefono: phone,
                                  genero: gender,
                                  edad: age,
                                  carData: usuariosDB.getCarsArrayAsDict(currentUsr.carros),
                                  facturas: currentUsr.facturas,
                                  transaccionActual: currentUsr.transaccionActual,
                                  familyGroupId: currentUsr.familyGroupId)
        
        UsuariosDB().editBasicDataForUserWithID(userToPatch.id, newUserData: userToPatch, callback: { successful, err in
            if successful {
                print("PATCH FOR USER WAS OKAY")
                _ = self.navigationController?.popViewController(animated: true)
            } else {
                print("ERROR PATCHING USER \(err!)")
            }
        })
    }
}

extension EditProfileViewController: UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    @IBAction func CancelEdition(_ sender: Any) {
         _ = navigationController?.popViewController(animated: true)
    }
     
     
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       if let nextField = textField.superview?.viewWithTag(textField.tag + 1) as? UITextField
       {
          nextField.becomeFirstResponder()
       }
       else
       {
          textField.resignFirstResponder()
       }
       return false
    }
        @objc func keyboardWillShow(notification: NSNotification) {
        if self.view.frame.origin.y == 0 {
            self.view.frame.origin.y -= 150
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
       if self.view.frame.origin.y != 0 {
        self.view.frame.origin.y = 0
       }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return genders.count
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return genders[row]
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        generoField.text = genders[row]
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       view.endEditing(true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var maxLength = 10
        if textField == nombreField
        {
            maxLength = 65
        }
        else if textField == edadField
        {
            maxLength = 3
        }
        else if textField == telefonoField
        {
            maxLength = 15
        }
        else if textField == cedulaField
        {
            maxLength = 15
        }
        else if textField == correoField
        {
            maxLength = 65
        }
        // get the current text, or use an empty string if that failed
        let currentText = textField.text ?? ""

        // attempt to read the range they are trying to change, or exit if we can't
        guard let stringRange = Range(range, in: currentText) else { return false }

        // add their new text to the existing text
        let updatedText = currentText.replacingCharacters(in: stringRange, with: string)

        // make sure the result is under 16 characters
        return updatedText.count <= maxLength
    }
}

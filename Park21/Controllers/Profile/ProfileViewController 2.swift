//
//  ProfileViewController.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var profileName: UITextField!
    @IBOutlet weak var profileGender: UITextField!
    @IBOutlet weak var profileAge: UITextField!
    @IBOutlet weak var profileCedula: UITextField!
    @IBOutlet weak var profileEmail: UITextField!
    @IBOutlet weak var profileContactNumber: UITextField!
    @IBOutlet weak var verCarrosButton: UIButton!
    @IBOutlet weak var familyGroupViewButton: UIButton!
    @IBOutlet weak var editarPerfilButton: UIBarButtonItem!
    @IBOutlet weak var cerrarSesionButton: UIButton!
    @IBOutlet weak var profileImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adjustUIFormat()
        setShownData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        updateUserData()
    }
    
    
    func updateUserData() {
        guard let userID = AppState.getState().usuario?.id else { return }
        UsuariosDB().getUserWithID(userID, callback: {data in
            if let newUsr = data {
                AppState.getState().usuario = newUsr
                self.setShownData()
            } else {
                print("Was unable to fetch new user data")
            }
        })
        
    }
    
    func setShownData() {
        guard let userData = AppState.getState().usuario else { return }
        profileName.text = userData.nombre
        profileGender.text = userData.genero
        profileAge.text = String(userData.edad)
        profileCedula.text = userData.cedula
        profileEmail.text = userData.correo
        profileContactNumber.text = userData.telefono
        if(userData.genero == "Femenino") {
            profileImage.image = #imageLiteral(resourceName: "defaultProfileFemale")
        } else {
            profileImage.image = #imageLiteral(resourceName: "defaultProfileMale")
        }
    }
    
    func adjustUIFormat() {
        ButtonFormatting.setNavigationBarGreen(self.navigationController!)
        ButtonFormatting.greenBorderTxtField(profileName)
        ButtonFormatting.greenBorderTxtField(profileAge)
        ButtonFormatting.greenBorderTxtField(profileCedula)
        ButtonFormatting.greenBorderTxtField(profileEmail)
        ButtonFormatting.greenBorderTxtField(profileContactNumber)
        ButtonFormatting.greenBorderTxtField(profileGender)
        ButtonFormatting.greenBorderButton(verCarrosButton)
        ButtonFormatting.greenBorderButton(familyGroupViewButton)
        ButtonFormatting.greenBorderButton(cerrarSesionButton)
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "EditarPerfil" {
            let destController = segue.destination as! EditProfileViewController
            destController.originalUserData = AppState.getState().usuario
        }
    }
    
    @IBAction func onFamilyGroup(_ sender: UIButton) {
        if let currentUser = AppState.getState().usuario {
            if currentUser.familyGroupId == "" {
                 performSegue(withIdentifier: "profileToFamilyOptions", sender: self)
            } else {
                 performSegue(withIdentifier: "ProfileToFamilyGroup", sender: self)
            }
        }
        else {
            print("Error con el usuario")
        }
    }
    
    @IBAction func onVerCarros(_ sender: UIButton) {
        performSegue(withIdentifier: "ProfileToCarsList", sender: self)
    }
    
    @IBAction func onEditarPerfil(_ sender: UIBarButtonItem) {
        if AppState.getState().networkStatus
        {
            performSegue(withIdentifier: "ProfileToEdit", sender: self)
        }
        else{
            performSegue(withIdentifier: "ProfileToEventualConnectivity", sender: self)
        }
    }
    
    @IBAction func onCerrarSesionClicked(_ sender: Any) {
        let yesAction: (UIAlertAction) -> Void = { action in
            //self.navigationController?.popToRootViewController(animated: true)
            
            AppState.clean()
            //Replace nav controller stack
            let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let welcomeVC : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "WelcomeView")
            if let navVC = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                navVC.setViewControllers([welcomeVC], animated: true)
            }
        }
        let noAction: (UIAlertAction) -> Void = {_ in }
        
        Alerts.showYesNoAlertWithHandlers(title: "Cerrar sesión", message: "Esta seguro que desea cerrar su sesión?", presenter: self, handlerYes: yesAction, handlerNo: noAction)
    }
}

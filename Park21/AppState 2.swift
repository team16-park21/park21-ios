//
//  State.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation

class AppState {
    //Singleton instance
    private static var state: AppState? = nil
    
    private var _usuario: Usuario? = nil
    var usuario: Usuario? {
        get { return self._usuario }
        set(val) { self._usuario = val }
    }
    
    private var _parqueaderos: [Parqueadero]? = nil
    var parqueaderos: [Parqueadero]? {
        get { return self._parqueaderos }
        set(val) { self._parqueaderos = val }
    }
    
    private init() {}
    
    //Get instance for singleton
    public static func getState() -> AppState {
        if let state = state {
            return state
        } else {
            state = AppState()
            return state!
        }
    }
}

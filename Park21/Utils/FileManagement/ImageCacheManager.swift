//
//  ImageCacheManager.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 11/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit

class ImageCacheManager {
    static let MAP_KEY = "ImageCacheMap"
    
    static func storeImage(imageId: String, image: UIImage) {
        let path = NSTemporaryDirectory().appending(UUID().uuidString)
        let url = URL(fileURLWithPath: path)
        
        let data = image.jpegData(compressionQuality: 0.5)
        try? data?.write(to: url)
        
        var dict = UserDefaults.standard.object(forKey: ImageCacheManager.MAP_KEY) as? [String: String]
        if dict == nil {
            dict = [String: String]()
        }
        
        dict![imageId] = path
        UserDefaults.standard.set(dict, forKey: ImageCacheManager.MAP_KEY)
    }
    
    static func isImageInCache(imageId: String) -> Bool {
        if let dict = UserDefaults.standard.object(forKey: ImageCacheManager.MAP_KEY) as? [String: String] {
            if let path = dict[imageId] {
                return true
            }
        }
        return false
    }
    
    static func getImage(imageId: String, callback: @escaping (UIImage) -> Void ) {
        if let dict = UserDefaults.standard.object(forKey: ImageCacheManager.MAP_KEY) as? [String: String] {
            if let path = dict[imageId] {
                if let data = try? Data(contentsOf: URL(fileURLWithPath: path)) {
                    if let img = UIImage(data: data) {
                        callback(img)
                    }
                }
            }
        }
    }
}

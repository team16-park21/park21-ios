//
//  NavControllerExtension.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 11/11/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import UIKit

extension UINavigationController {
    func deleteInBetweenTopAndClassFromStack(ofClass: AnyClass, animated: Bool) {
        var newStack = [UIViewController]()
        var flag = false
        for view in viewControllers {
            if !flag {
                if view.isKind(of: ofClass) {
                    flag = true
                    newStack.append(view)
                } else {
                    newStack.append(view)
                }
            }
        }
        newStack.append(viewControllers[viewControllers.count - 1])
        self.setViewControllers(newStack, animated: animated)
    }
    
    func popToViewController(ofClass: AnyClass, animated: Bool) {
        if let vc = viewControllers.last(where: { $0.isKind(of: ofClass) }) {
            popToViewController(vc, animated: animated)
        }
    }
}

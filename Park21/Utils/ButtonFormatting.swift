//
//  ButtonFormatting.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/11/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit

class ButtonFormatting {
    
    static func greenBorderButton (_ button: UIButton)
    {
        button.backgroundColor = #colorLiteral(red: 0.3667953312, green: 0.7296931148, blue: 0.7510977983, alpha: 0)
        button.layer.cornerRadius = 10;
        button.layer.borderWidth = 1.5;
        button.layer.backgroundColor = #colorLiteral(red: 0.3667953312, green: 0.7296931148, blue: 0.7510977983, alpha: 1);
        button.layer.borderColor = #colorLiteral(red: 0.3667953312, green: 0.7296931148, blue: 0.7510977983, alpha: 1);
    }
    
    static func grayBorderButton (_ button: UIButton)
    {
        button.backgroundColor = #colorLiteral(red: 0.3667953312, green: 0.7296931148, blue: 0.7510977983, alpha: 0)
        button.layer.cornerRadius = 10;
        button.layer.borderWidth = 1.5;
        button.layer.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1);
        button.layer.borderColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1);
    }
    
    //    static func purpleBorderButton (_ button: UIButton)
    //    {
    //        button.backgroundColor = #colorLiteral(red: 0.3667953312, green: 0.7296931148, blue: 0.7510977983, alpha: 0)
    //        button.layer.cornerRadius = 10;
    //        button.layer.borderWidth = 1.5;
    //        button.layer.backgroundColor = #colorLiteral(red: 0.6047298908, green: 0.3485313952, blue: 0.4518780112, alpha: 1);
    //        button.layer.borderColor = #colorLiteral(red: 0.6047298908, green: 0.3485313952, blue: 0.4518780112, alpha: 1);
    //    }
    
    static func greenBorderTxtField (_ txt: UITextField)
    {
        txt.layer.cornerRadius = 10;
        txt.layer.borderWidth = 1.5;
        txt.layer.borderColor = #colorLiteral(red: 0.3667953312, green: 0.7296931148, blue: 0.7510977983, alpha: 1);
    }
    
    //    static func purpleBorderTxtField (_ txt: UITextField)
    //    {
    //        txt.layer.cornerRadius = 10;
    //        txt.layer.borderWidth = 1.5;
    //        txt.layer.borderColor = #colorLiteral(red: 0.6047298908, green: 0.3485313952, blue: 0.4518780112, alpha: 1);
    //    }
    
    static func setLogoAsTitle (_ nav: UINavigationItem )
    {
//        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
//        imageView.contentMode = .scaleAspectFit
//        let image = UIImage(named: "Park21v.png")
//        imageView.image = image
//        nav.titleView = imageView
    }
    
    static func setNavigationBarGreen (_ nav: UINavigationController)
    {
        let navi = nav.navigationBar
        nav.navigationItem.setHidesBackButton(true, animated: true)
        let col = #colorLiteral(red: 0.3667953312, green: 0.7296931148, blue: 0.7510977983, alpha: 1)
        navi.tintColor = col
    }
    
    //    static func setNavigationBarPurple (_ nav: UINavigationController)
    //    {
    //        let navi = nav.navigationBar
    //        nav.navigationItem.setHidesBackButton(true, animated: true)
    //        let col = #colorLiteral(red: 0.6047298908, green: 0.3485313952, blue: 0.4518780112, alpha: 1)
    //        navi.tintColor = col
    //    }
}

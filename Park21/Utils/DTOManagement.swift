//
//  DTOManagement.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/18/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation

class DTOManagement {
    func filterDict(dict: [String: Any], filters: [String]) -> [String: Any] {
        var newDict = [String: Any]()
        for f in filters {
            newDict[f] = dict[f]
        }
        return newDict
    }
}

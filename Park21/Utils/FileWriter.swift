//
//  FileWriter.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/16/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation

class FileManagementUtil {
    func writeToFile(file: String, payload: String) {

        if let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {

            let fileURL = dir.appendingPathComponent(file)

            //writing
            do {
                try payload.write(to: fileURL, atomically: false, encoding: .utf8)
            }
            catch {/* error handling here */}

            //reading
            do {
                let text2 = try String(contentsOf: fileURL, encoding: .utf8)
                print("\(text2)")
            }
            catch {/* error handling here */}
            
            print("TEXT 2 LMAO")
        }
    }
}

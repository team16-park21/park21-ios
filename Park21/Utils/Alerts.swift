//
//  Alerts.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/12/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit

class Alerts {
    public static let DEFAULT_DISMISS_TIME = 1.5
    
    //Textos default
    static let errorTitle = "Error"
    static let aceptarTitle = "Aceptar"
    static let ignorarTitle = "Ignorar"
    static let advertenciaTitle = "Advertencia"
    static let operacionExitosaTitle = "Operación Exitosa"
    static let needsPermissons = "Permisos insuficientes"
    static let confirmacionTitle = "Confirmación"
    static let errorConexion = "Error de conexión"
    static let disponibleProximamenteTitle = "Disponible próximamente!"
    static let noCarsMessage = "Recuerda que debes registrar al menos un carro para poder parquear en alguno de nuestros parqueaderos. Deseas crear un carro ahora?"
    static let disponibleProximamenteMessage = "Esta función estara disponible próximamente. Agradecemos tu paciencia, estamos haciendo todo lo posible para incorporarla lo antes posible."
    
    public static func showErrorAlert(_ message: String, presenter: UIViewController){
        let alert = UIAlertController(title: Alerts.errorTitle, message: message, preferredStyle: UIAlertController.Style.alert)
            
        alert.addAction(UIAlertAction(title: Alerts.aceptarTitle, style: UIAlertAction.Style.default, handler: nil))
        presenter.present(alert, animated: true, completion: nil)
    }
    
    public static func showOperationSuccessful(presenter: UIViewController, message: String, handler: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: Alerts.operacionExitosaTitle, message: message, preferredStyle: UIAlertController.Style.alert)
            
        alert.addAction(UIAlertAction(title: Alerts.aceptarTitle, style: UIAlertAction.Style.default, handler: handler))
        presenter.present(alert, animated: true, completion: nil)
    }
    
    public static func showNoCarsAlert(presenter: UIViewController, handler: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: Alerts.advertenciaTitle, message: Alerts.noCarsMessage, preferredStyle: UIAlertController.Style.alert)
            
        alert.addAction(UIAlertAction(title: Alerts.ignorarTitle, style: UIAlertAction.Style.default, handler: nil))
        alert.addAction(UIAlertAction(title: "Crear", style: UIAlertAction.Style.default, handler: handler))
        presenter.present(alert, animated: true, completion: nil)
    }
    
    public static func showNeedsPermissonsAlert(_ message: String, presenter: UIViewController){
        let alert = UIAlertController(title: Alerts.needsPermissons, message: message, preferredStyle: UIAlertController.Style.alert)
            
        alert.addAction(UIAlertAction(title: Alerts.aceptarTitle, style: UIAlertAction.Style.default, handler: nil))
        presenter.present(alert, animated: true, completion: nil)
    }
    
    public static func showMessageAlertWithHandler(_ message: String, presenter: UIViewController, handler: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: Alerts.confirmacionTitle, message: message, preferredStyle: UIAlertController.Style.alert)
            
        alert.addAction(UIAlertAction(title: Alerts.aceptarTitle, style: UIAlertAction.Style.default, handler: handler))
        presenter.present(alert, animated: true, completion: nil)
    }
    
    public static func showMessageAlert(_ message: String, presenter: UIViewController){
        let alert = UIAlertController(title: Alerts.confirmacionTitle, message: message, preferredStyle: UIAlertController.Style.alert)
            
        alert.addAction(UIAlertAction(title: Alerts.aceptarTitle, style: UIAlertAction.Style.default, handler: nil))
        presenter.present(alert, animated: true, completion: nil)
    }
    
    public static func showConnectionErrorAlert( presenter: UIViewController, message: String){
        let alert = UIAlertController(title: Alerts.errorConexion, message: message, preferredStyle: UIAlertController.Style.alert)
               
        alert.addAction(UIAlertAction(title: Alerts.aceptarTitle, style: UIAlertAction.Style.default, handler: nil))
           presenter.present(alert, animated: true, completion: nil)
    }
    
    public static func showYesNoAlertWithHandlers(title: String, message: String, presenter: UIViewController, handlerYes: @escaping (UIAlertAction) -> Void, handlerNo: @escaping (UIAlertAction) -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
        alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: handlerNo))
        alert.addAction(UIAlertAction(title: "Sí", style: UIAlertAction.Style.default, handler: handlerYes))
        
        presenter.present(alert, animated: true, completion: nil)
    }
    
    public static func showInProgressView(_ view: UIViewController, message: String, onCompletion: (() -> Void)?) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)

        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating();

        alert.view.addSubview(loadingIndicator)
        view.present(alert, animated: true, completion: onCompletion)
    }
    
    public static func stopInProgressView(_ view: UIViewController) {
        view.dismiss(animated: true, completion: nil)
    }
    
    public static func commingSoonAlert( presenter: UIViewController)  {
        let alert = UIAlertController(title: Alerts.disponibleProximamenteTitle, message: Alerts.disponibleProximamenteMessage, preferredStyle: UIAlertController.Style.alert)
            
        alert.addAction(UIAlertAction(title: Alerts.aceptarTitle, style: UIAlertAction.Style.default, handler: nil))
        presenter.present(alert, animated: true, completion: nil)
    }
}


//
//  ParqueaderosObserver.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 11/13/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Firebase

class ParqueaderosObserver {
    var parqueaderosListener: ListenerRegistration? = nil
    var onDismissCallback: () -> Void
    var onUpdateCallback: ([Parqueadero]) -> Void
    
    init(onDismissCallback: @escaping () -> Void, onUpdateCallback: @escaping ([Parqueadero]) -> Void) {
        self.onDismissCallback = onDismissCallback
        self.onUpdateCallback = onUpdateCallback
        
        parqueaderosListener = ParqueaderosDB().createObserverToParqueaderos(onSnapshotCallback: { parqueaderos in
            self.onUpdateCallback(parqueaderos)
        })
    }
    
    func dismiss() {
        self.parqueaderosListener?.remove()
        self.parqueaderosListener = nil
        onDismissCallback()
    }
}

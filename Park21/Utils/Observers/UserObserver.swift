//
//  UserObserver.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 11/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Firebase

class UserTransactionObserver {
    var userObserver: ListenerRegistration? = nil
    var onDismissCallback: () -> Void
    
    init(onDismissCallback: @escaping () -> Void) {
        self.onDismissCallback = onDismissCallback
        guard let userId = AppState.getState().usuario?.id else { return }
        userObserver = UsuariosDB().createObserverToUserWithID(userId, onSnapshotCallback: { usr in
            self.refreshUserState(usr)
            self.manageTransaction(usr)
        })
    }
    
    func refreshUserState(_ user: Usuario) {
        AppState.getState().usuario = user
    }
    
    func manageTransaction(_ user: Usuario) {
        //Check for transaction
        if user.transaccionActual != "" {
            AppState.getState().transactionIsPayed = false
            TransaccionesDB().getTransaccionWithID(user.transaccionActual, callback: {data in
                if let transaccion = data {
                    print("Transaction is OK")
                    
                    //Dismiss observer to avoid memory leaks
                    self.dismiss()
                    
                    //Update state
                    AppState.getState().userInTransaction = true
                    AppState.getState().currentTransaction = transaccion
                    
                    //Replace current vc with mainParkingVC
                    let mainStoryboardIpad : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    let parkingVC : UIViewController = mainStoryboardIpad.instantiateViewController(withIdentifier: "MainParkingView")
                    if let navVC = UIApplication.shared.keyWindow?.rootViewController as? UINavigationController {
                        navVC.setViewControllers([parkingVC], animated: true)
                    } else {
                        print("Cant fetch navController")
                    }
                } else {
                    print("Unable to fetch a transaction with the ID \(user.transaccionActual)")
                }
            })
        }
    }
    
    func dismiss() {
        self.userObserver?.remove()
        self.userObserver = nil
        self.onDismissCallback()
    }
}

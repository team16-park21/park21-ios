//
//  FamiliaObserver.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 11/10/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Firebase

class FamiliaObserver {
    var familiaListener: ListenerRegistration? = nil
    var onDismissCallback: () -> Void
    var onUpdateCallback: (Familia) -> Void
    
    init(onDismissCallback: @escaping () -> Void, onUpdateCallback: @escaping (Familia) -> Void) {
        self.onDismissCallback = onDismissCallback
        self.onUpdateCallback = onUpdateCallback
        guard let user = AppState.getState().usuario else {
            onDismissCallback()
            return
        }
        
        if user.familyGroupId == "" {
            onDismissCallback()
            return
        }
        
        self.familiaListener = FamiliaDB().createObserverToFamilyWithID(user.familyGroupId, onSnapshotCallback: { familia in
            self.onUpdateCallback(familia)
        })
    }
    
    func dismiss() {
        self.familiaListener?.remove()
        self.familiaListener = nil
        self.onDismissCallback()
    }
}


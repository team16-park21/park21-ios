//
//  ConnectionValidator.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/14/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Network
import UIKit

class ConnectionValidator: UIViewController{

    var networkStatur = false
    
    func checkConnectivityToAlert (_ segue: String, presenter: UIViewController, message: String)
    {
        if AppState.getState().networkStatus{
            performSegue(withIdentifier: segue, sender: presenter)
        }
        else {
            Alerts.showConnectionErrorAlert(presenter: presenter, message: message)
        }
    }
    
    func checkConnectivityToView (_ segue: String, presenter: UIViewController, message: String)
    {
        if AppState.getState().networkStatus{
            performSegue(withIdentifier: segue, sender: presenter)
        }
        else {
            Alerts.showConnectionErrorAlert(presenter: presenter, message: message)
        }
    }
}

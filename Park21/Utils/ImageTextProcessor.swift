//
//  ImageTextProcessor.swift
//  Park21
//
//  Created by Jorge Andres Gomez Villamizar on 10/31/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import Firebase

class ImageTextProcessor {
    //Text recognition variables
    let vision = Vision.vision()
    var textRecognizer: VisionTextRecognizer!
    
    init() {
        textRecognizer = vision.onDeviceTextRecognizer()
    }
    
    func process(_ image: UIImage, callback: @escaping (_ blocks: [VisionTextBlock]) -> Void) {
        var visionImage: VisionImage
        if let rotatedImage = rotate(input: image, radians: Float.pi / 4) {
            print("IMAGE WAS ROTATED!")
            visionImage = VisionImage(image: rotatedImage)
        } else {
            visionImage = VisionImage(image: image)
        }
        //visionImage.metadata?.orientation = .topRight
        
        textRecognizer.process(visionImage) { result, error in
            //Check for errors
            guard error == nil, let result = result else {
                callback([])
                return
            }
            // return results
            callback(result.blocks)
        }
    }
    
    func createFacturaFromBlocks(_ blocks: [VisionTextBlock]) -> Factura {
        var pValor = 0
        var pEstablecimiento = ""
        let pFecha = Date()
        
        for block in blocks {
            print("CHECKING BLOCK == \(block.text)")
            if let valorAsInt = matchesValor(block.text, currentVal: pValor) {
                pValor = valorAsInt
            } else if let establecimientoTxt = matchesEstablecimiento(block.text) {
                pEstablecimiento = establecimientoTxt
            }
        }
        
        //print("FACTURA WITH VALUE == \(pValor)")
        //print("FACTURA WITH ESTBL == \(pEstablecimiento)")
        return Factura(id: "", valor: pValor, establecimiento: pEstablecimiento, fecha: pFecha, userId: "")
    }
    
    func matchesValor(_ txt: String, currentVal: Int) -> Int? {
        let isANumberRegex = "\\$?[0-9\\.]"
        if let result = txt.range(of: isANumberRegex, options: .regularExpression)
        {
            let newVal = cleanTxtToNumber(txt)
            if newVal > currentVal {
                return newVal
            }
        }
        
        let totalValRegex = "TOTAL[a-z A-Z]*\\$?[0-9\\.]+"
        if let result = txt.range(of: totalValRegex, options: .regularExpression)
        {
            let newVal = cleanTxtToNumber(txt)
            if newVal > currentVal {
                return newVal
            }
        }
        
        return nil
    }
    
    func cleanTxtToNumber(_ txt: String) -> Int {
        let numbers = ["0","1","2","3","4","5","6","7","8","9"]
        var numChars = ""
        for s in txt {
            if numbers.contains(String(s)) {
                numChars.append(s)
            }
        }
        
        //print("NUM CHARS CONFIG == \(numChars)")
        if numChars.count > 5 || numChars == "" {
            return 0
        }
        return Int(numChars)!
    }
    
    func matchesEstablecimiento(_ txt: String) -> String? {
        if txt.contains("SA") || txt.contains("S.A") || txt.contains("$.A") || txt.contains("$AS") || txt.contains("SA$") {
            return txt
        }
        return nil
    }
    
    func matchesFecha(_ txt: String) -> String? {
        return nil
    }
    
    func rotate(input: UIImage, radians: Float) -> UIImage? {
        var newSize = CGRect(origin: CGPoint.zero, size: input.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)

        UIGraphicsBeginImageContextWithOptions(newSize, false, input.scale)
        let context = UIGraphicsGetCurrentContext()!

        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        input.draw(in: CGRect(x: -input.size.width/2, y: -input.size.height/2, width: input.size.width, height: input.size.height))

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage
    }
}

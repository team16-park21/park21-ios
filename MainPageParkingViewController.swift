//
//  MainPageParkingViewController.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import UIKit
class MainPageParkingViewController: UIViewController {
    @IBOutlet weak var imageViewParqueadero: UIImageView!
    
    @IBOutlet weak var GetMeToMyCarButton: UIButton!
    @IBOutlet weak var PayParkingButton: UIButton!
    @IBOutlet weak var SeeMyCarButton: UIButton!
    @IBOutlet weak var ExitParkingButton: UIButton!
    @IBOutlet weak var ProfileButton: UIBarButtonItem!
    
    private var _parqueaderoImage: UIImage? = nil
    private var payed = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.setHidesBackButton(true, animated: true)
        
        setParqueaderoImage()
        updateParqueaderoData()
        updateParqueaderoImage()
        styleButtons()
        
        AppState.getState().dumpState()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: animated)
        payParkingButtonStatus()
    }
    
    func styleButtons() {
        ButtonFormatting.greenBorderButton(GetMeToMyCarButton)
        ButtonFormatting.greenBorderButton(PayParkingButton)
        ButtonFormatting.greenBorderButton(SeeMyCarButton)
        if AppState.getState().transactionIsPayed {
            ButtonFormatting.greenBorderButton(ExitParkingButton)
        } else {
            ButtonFormatting.grayBorderButton(ExitParkingButton)
        }
        ButtonFormatting.setNavigationBarGreen(self.navigationController!)
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
    }
    
    func updateParqueaderoData() {
        guard let transaccion = AppState.getState().currentTransaction else {
            print("ERROR WITH STATE, NO VALID TRANSACTION")
            return
        }
        
        let idParqueadero = transaccion.parqueaderoID
        guard let parqueaderos = AppState.getState().parqueaderos else {
            print("ERROR WITH STATE, NO PARQUEADEROS LISTED")
            return
        }
        
        for entity in parqueaderos {
            if entity.id == idParqueadero {
                AppState.getState().currentParqueadero = entity
            }
        }
    }
    
    func updateParqueaderoImage() {
        guard let parqueadero = AppState.getState().currentParqueadero else { return }
        ParqueaderosDB().getImageForParqueadero(parqueadero: parqueadero, quality: ParqueaderosDB.TRANSACTION_QUALITY, callback: { image in
            self._parqueaderoImage = image
            self.setParqueaderoImage()
        })
    }
    
    func setParqueaderoImage() {
        if let image = _parqueaderoImage {
            imageViewParqueadero.image = image
        } else {
            imageViewParqueadero.image = ParqueaderosDB().getDefaultParqueaderoImage()
        }
    }
    
    func payParkingButtonStatus()
    {
        if AppState.getState().transactionIsPayed {
            ExitParkingButton.isEnabled = true
            ButtonFormatting.greenBorderButton(ExitParkingButton)
        } else {
            ExitParkingButton.isEnabled = false
            ButtonFormatting.grayBorderButton(ExitParkingButton)
        }
    }
    
    @IBAction func onSeeMyCarClicked(_ sender: Any) {
        if AppState.getState().networkStatus{
            performSegue(withIdentifier: "ParkingMainToSnapshot", sender: self)
        }
        else {
            //            Mark change text of the view
            performSegue(withIdentifier: "ParkingMainToEventualConectivity", sender: self)
        }
    }
    
    @IBAction func onPayParkingClicked(_ sender: Any) {
        if AppState.getState().networkStatus{
            performSegue(withIdentifier: "ParkingMainToPay", sender: self)
            
        }
//            Mark change text of the view
        else{
            performSegue(withIdentifier: "ParkingMainToEventualConectivity", sender: self)
        }
    }
    
    @IBAction func onGetMeToMyCarClicked(_ sender: Any) {
        performSegue(withIdentifier: "MainParkingToHowToCar", sender: self)
    }
    
    @IBAction func onExitParkingClicked(_ sender: Any) {
        performSegue(withIdentifier: "ParkingMainToExitParking", sender: self)
    }
    
    @IBAction func onProfileButtonClicked(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "MainPageToProfile", sender: self)
    }
}

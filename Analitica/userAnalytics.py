import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

#¿Cuál es la edad promedio de los usuarios que entran al parqueadero en una franja de tiempo?
#Dada una franja horaria, ¿Cuál es el género que más ingresa al centro comercial en vehículo?
#¿Cuál es el promedio de tiempo que pasan las personas en el centro comercial ?
#¿Cual es el género que registra más transacciones dada una franja de tiempo?
#¿Cuáles son las horas en las que entran más vehículos?

dfUsuarios = pd.DataFrame(columns=['dbUserID', 'cedula', 'edad', 'genero', 'numCarros'])
dfCarros = pd.DataFrame(columns=['placa', 'marca', 'modelo'])
dfTransaccion = pd.DataFrame(columns=['transID','parqueadero', 'usuario'])
dfParqueaderos = pd.DataFrame(columns=['parkID','nombre'])
join = pd.DataFrame()


def usersToDataFrame(users):
	global dfUsuarios, dfCarros
	dfUsuarios = pd.DataFrame(columns=['dbUserID', 'cedula', 'edad', 'genero', 'numCarros'])
	dfCarros = pd.DataFrame(columns=['placa', 'marca', 'modelo'])
	for u in users:
		dfUsuarios = dfUsuarios.append({'dbUserID': u, 'cedula':users[u]['cedula'], 'edad':users[u]['edad'], 'genero':users[u]['genero'], 'numCarros':len(users[u]['carros'])}, ignore_index=True)
		for carros in users[u]['carros']:
			dfCarros = dfCarros.append({'placa': carros['placa'], 'marca': carros['marca'], 'modelo': carros['modelo']}, ignore_index=True)

def parkingsToDataFrame(parkings):
	global dfParqueaderos
	dfParqueaderos = pd.DataFrame(columns=['parkID','nombre'])
	for parking in parkings:
		dfParqueaderos = dfParqueaderos.append({'parkID': parking,'nombre': parkings[parking]['nombre']}, ignore_index=True)

def transactionsToDataFrame(transacciones):
	global dfTransaccion,join
	dfTransaccion = pd.DataFrame(columns=['transID','parqueadero', 'usuario'])
	join = pd.DataFrame()
	for transaction in transacciones:
		dfTransaccion = dfTransaccion.append({'transID': transaction,'parqueadero': transacciones[transaction]['parqueadero'], 'usuario': transacciones[transaction]['usuario']}, ignore_index=True)
	joinUser = dfTransaccion.set_index('usuario').join(dfUsuarios.set_index('dbUserID'))[['transID', 'parqueadero', 'cedula']]
	join = joinUser.set_index('parqueadero').join(dfParqueaderos.set_index('parkID'))[['transID', 'nombre', 'cedula']]

#Analytics question #1: dsitribution of ages
def ageDistributionUsers():
    ageDist = dfUsuarios.edad.value_counts()
    ageDist.columns = ['edad', 'conteo']
    ageDist.plot(kind='bar', x='edad', y='conteo', figsize=(20, 18))
    plt.savefig('static/ageDistribution.png')

#Analytics question #2: dsitribution of gender
def genderDistributionUsers():
	genderDist = dfUsuarios.genero.value_counts()
	genderDist.columns = ['genero', 'conteo']
	genderDist.plot(kind='bar', x='gender', y='conteo', figsize=(20, 18))
	plt.savefig('static/genderDistribution.png')

#Analytics question #3: dsitribution of age by gender
def ageDistributionbygender():
	dfUsuarios.groupby(['genero', 'edad'] )['edad'].count().plot.bar(figsize=(20, 18))
	plt.savefig('static/ageGenderDistribution.png')

#Analytics question #4: avarage number of cars per user
def distributionNumberofCarsperUser():
	dfUsuarios.groupby('numCarros')['dbUserID'].count().plot.bar(figsize=(20,18))
	plt.savefig('static/amountofuserswithnumberofcars.png')

#Analytics question #5: distribution of car's brands
def distributionOfCarsBrands():
	dfCarros.groupby('marca')['placa'].count().plot.bar(figsize=(20,18))
	plt.savefig('static/brandsDistribution.png')

#Analytics question #6: distribution of car's models
def distributionOfCarsModels():
	dfCarros.groupby('modelo')['placa'].count().plot.bar(figsize=(20,18))
	plt.savefig('static/modelsDistribution.png')

#Analytics question #7: distribution of car's models by brand
def distributionOfCarsModelsByBrands():
	dfCarros.groupby(['marca', 'modelo'] )['modelo'].count().plot.bar(figsize=(20, 18))
	plt.savefig('static/modelBrandDistribution.png')

#Analytics question #8: distribution of transactions per User
def transaccionsPerUser():
	join.groupby('cedula')['transID'].count().plot.bar(figsize=(20,18))
	plt.savefig('static/transactionsPerUser.png')

#Analytics question #9: distribution of transactions per parking
def transactionsPerParking():
	join.groupby('nombre')['transID'].count().plot.bar(figsize=(20,18))
	plt.savefig('static/transactionsPerParking.png')

#Analytics question #10: distribution of transactions per parking by user
def transactionsPerUserbyParking():
	join.groupby(['nombre', 'cedula'] )['transID'].count().plot.bar(figsize=(20, 18))
	plt.savefig('static/transactionsPerUserbyParking.png')

#Organize informatión for the users report
def getUsersReport():
	ageDistributionUsers()
	genderDistributionUsers()
	ageDistributionbygender()
	distributionNumberofCarsperUser()
	transaccionsPerUser()

#Organize informatión for the parkings report
def getParkingsReport():
	transactionsPerParking()
	transactionsPerUserbyParking()

#Organize informatión for the cars report
def getCarsReport():
	distributionOfCarsBrands()
	distributionOfCarsModels()
	distributionOfCarsModelsByBrands()
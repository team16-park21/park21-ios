import firebase_admin
import userAnalytics as analytics
from firebase_admin import credentials
from firebase_admin import firestore
from flask import Flask, render_template

# Use a service account
if (not len(firebase_admin._apps)):
    cred = credentials.Certificate('park-21-firebase-adminsdk-4s36e-a819442358.json') 
    default_app = firebase_admin.initialize_app(cred)
db = firestore.client()

#Rest api
app = Flask(__name__)

@app.route('/')
def index():
    obtenerDataFrames()
    analytics.getUsersReport()
    analytics.getCarsReport()
    analytics.getParkingsReport()
    return render_template('mainPage.html')

@app.route('/usuarios')    
def usuarios():
    return render_template('users.html')  

@app.route('/carros')
def carros():
    return render_template('carros.html')

@app.route('/parqueaderos')
def parqueaderos():
    return render_template('parking.html')    

def obtenerDataFrames():
    usuarios ={}
    transacciones ={}
    parqueaderos ={}

    users_ref = db.collection('usuarios')
    docs = users_ref.stream()
    for doc in docs:
        usuarios[doc.id] = doc.to_dict()
    analytics.usersToDataFrame(usuarios)

    parking_ref = db.collection('parqueaderos')
    docs = parking_ref.stream()
    for doc in docs:
           parqueaderos[doc.id] = doc.to_dict()
    analytics.parkingsToDataFrame(parqueaderos)

    transaction_ref = db.collection('transacciones')
    docs = transaction_ref.stream()
    for doc in docs:
        transacciones[doc.id] = doc.to_dict()
    analytics.transactionsToDataFrame(transacciones)

    
#Main method
if __name__ == '__main__':
	app.run(port=5002, debug=True)
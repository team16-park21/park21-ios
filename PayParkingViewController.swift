//
//  PayParkingViewController.swift
//  Park21
//
//  Created by Maria del Rosario León on 10/6/19.
//  Copyright © 2019 PARK21. All rights reserved.
//

import Foundation
import AVFoundation
import UIKit

class PayParkingViewController: UIViewController {
    //Alertas
    let permisoCameraMessage = "Es necesario que la aplicación tenga acceso a la cámara para poder registrar facturas. Por favor habilite el acceso para hacer uso de esta funcionalidad."
    let pagoRealizadoMessage = "El pago ha sido realizado correctamente."
    
    @IBOutlet weak var plateTxtField: UILabel!
    @IBOutlet weak var parkingIdTxtField:
    UILabel!
    @IBOutlet weak var registrationTimeTxtField: UILabel!
    @IBOutlet weak var valueTxtField: UILabel!
    @IBOutlet weak var discountTxtField: UILabel!
    @IBOutlet weak var payCreditCardButton: UIButton!
    @IBOutlet weak var payPSEButton: UIButton!
    @IBOutlet weak var InvoiceRegistrationButton: UIButton!
    
    var imagePicker: UIImagePickerController!
    var image: UIImage!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        styleView()
        showData()
    }
    
    func showData() {
        defaultValues()
        
        if let carros = AppState.getState().usuario?.carros {
            if carros.count > 0 {
                plateTxtField.text = carros[0].placa
            }
        }
        
        if let transaction = AppState.getState().currentTransaction {
            parkingIdTxtField.text = transaction.puestoParqueo?.id_puesto
            let calendar = Calendar.current
            if let fecha = transaction.fechaEntrada {
                let info = calendar.dateComponents([.hour, .minute], from: fecha)
                
                let minute = info.minute!
                if minute < 10 {
                    registrationTimeTxtField.text = "\(info.hour!):0\(minute)"
                } else {
                    registrationTimeTxtField.text = "\(info.hour!):\(minute)"
                }
            }
        }
    }
    
    func defaultValues() {
        //Default values
        plateTxtField.text = "GKY591"
        registrationTimeTxtField.text = "12:35"
        valueTxtField.text = "12350 COP"
        discountTxtField.text = "0 COP"
        parkingIdTxtField.text = "E27"
    }
    
    func styleView(){
        ButtonFormatting.setLogoAsTitle(self.navigationItem)
        ButtonFormatting.setNavigationBarGreen(self.navigationController!)
        ButtonFormatting.greenBorderButton(payCreditCardButton)
        ButtonFormatting.greenBorderButton(payPSEButton)
        ButtonFormatting.greenBorderButton(InvoiceRegistrationButton)
    }
    
    @IBAction func onPayPSEClicked(_ sender: UIButton) {
        AppState.getState().transactionIsPayed = true
        if AppState.getState().networkStatus{
            if let url = URL(string: "https://www.pse.com.co/inicio") {
                UIApplication.shared.open(url)
                Alerts.showMessageAlert(pagoRealizadoMessage, presenter: self)
            }
        } else {
            Alerts.showConnectionErrorAlert(presenter: self, message: "No podemos abrir la página para realizar el pago por falta de conexión a internet. Inténtalo más tarde.")
        }
    }
    
    @IBAction func onPayCreditCardClicked(_ sender: UIButton) {
        AppState.getState().transactionIsPayed = true
        Alerts.showMessageAlert(pagoRealizadoMessage, presenter: self)
        //Alerts.commingSoonAlert(presenter: self)
    }
}

extension PayParkingViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    @IBAction func onInvoiceRegistrationClicked(_ sender: UIButton) {
        if AVCaptureDevice.authorizationStatus(for: .video) ==  .authorized {
            imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        } else {
            AVCaptureDevice.requestAccess(for: .video, completionHandler: { (granted: Bool) in
                if !granted {
                    Alerts.showNeedsPermissonsAlert(self.permisoCameraMessage, presenter: self)
                }
            })
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        image = info[.originalImage] as? UIImage
        generateImageData(image, callback: {
            self.imagePicker.dismiss(animated: true, completion: nil)
            self.performSegue(withIdentifier: "PayToInvoiceInfo", sender: self)
        })
    }
    
    func generateImageData(_ image: UIImage, callback: @escaping () -> Void) {
        let factory = ImageTextProcessor()
        factory.process(image, callback: { blocks in
            let factura = factory.createFacturaFromBlocks(blocks)
            AppState.getState().factura = factura
            callback()
        })
    }
}
